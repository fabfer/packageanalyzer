package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import util.RUtil;

public class Analysis {

	public static final String FIELD_NAME_UUID = "uuid";
	public static final String FIELD_NAME_REASON = "reason";
	public static final String FIELD_NAME_FAST_R_VERSION = "fast_r_version";
	public static final String FIELD_NAME_R_VERSION = "r_version";
	public static final String FIELD_NAME_DATETIME = "datetime";

	private List<AnalyzedPackage> analyzedPackages;

	private String uuid;
	private String reason;
	private String fastRVersion;
	private String rVersion;
	private Timestamp datetime;

	private Analysis() {
		analyzedPackages = new ArrayList<>();
	}

	public static Analysis createInstance(SQLSession sqlSession, String reason) {
		Analysis analysis = new Analysis();
		analysis.uuid = UUID.randomUUID().toString();
		analysis.reason = reason;
		analysis.rVersion = RUtil.R_VERSION;
		analysis.fastRVersion = RUtil.FAST_R_VERSION;
		analysis.datetime = new Timestamp(new Date().getTime());
		if (!analysis.persist(sqlSession))
			System.out.println("Unable to persist " + PackageAnalyzerDB.NAME_ANALYSIS + " into DB");
		return analysis;
	}

	public static Analysis getLastInstanceBeforeNow(SQLSession sqlSession) {
		return getLastInstanceBeforeDate(sqlSession, new Date());
	}

	public static Analysis getLastInstanceBeforeDate(SQLSession sqlSession, Date date) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_ANALYSIS + " WHERE " + FIELD_NAME_DATETIME + " < ? ORDER BY "
				+ FIELD_NAME_DATETIME + " DESC FETCH FIRST ROW ONLY";
		ResultSet rs = null;
		Analysis analysis = null;
		if (date != null) {
			try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
				int index = 1;
				stmt.setTimestamp(index++, new Timestamp(date.getTime()));
				rs = stmt.executeQuery();
				analysis = getAnalysis(sqlSession, rs);
				analysis.analyzedPackages = AnalyzedPackage.getAll(sqlSession, analysis);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				PackageAnalyzerDB.close(rs);
			}
		}
		return analysis;
	}

	private static Analysis getAnalysis(SQLSession sqlSession, ResultSet rs) throws SQLException {
		Analysis analysis = null;
		if (rs != null && rs.next()) {
			try {
				if (PackageAnalyzerDB.NAME_ANALYSIS.equals(rs.getMetaData().getTableName(1).toLowerCase())) {
					analysis = new Analysis();
					analysis.uuid = rs.getString(FIELD_NAME_UUID);
					analysis.reason = rs.getString(FIELD_NAME_REASON);
					analysis.rVersion = rs.getString(FIELD_NAME_R_VERSION);
					analysis.fastRVersion = rs.getString(FIELD_NAME_FAST_R_VERSION);
					analysis.datetime = rs.getTimestamp(FIELD_NAME_DATETIME);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return analysis;
	}

	public boolean persist(SQLSession sqlSession) {
		String sql = "INSERT INTO " + PackageAnalyzerDB.NAME_ANALYSIS + " VALUES (?, ?, ?, ?, ?)";
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, uuid);
			stmt.setString(index++, reason);
			stmt.setString(index++, fastRVersion);
			stmt.setString(index++, rVersion);
			stmt.setTimestamp(index++, datetime);
			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 1) {
				sqlSession.commit();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<AnalyzedPackage> getAnalyzedPackages() {
		return analyzedPackages;
	}

	public String getUuid() {
		return uuid;
	}

	public String getReason() {
		return reason;
	}

	public String getFastRVersion() {
		return fastRVersion;
	}

	public String getRVersion() {
		return rVersion;
	}

	public Timestamp getDatetime() {
		return datetime;
	}

	public void addAnalyzedPackage(AnalyzedPackage analyzedPackage) {
		analyzedPackages.add(analyzedPackage);
	}

	public boolean delete(SQLSession sqlSession) {
		String sql = "DELETE FROM " + PackageAnalyzerDB.NAME_ANALYSIS + " WHERE " + FIELD_NAME_UUID + " = ?";
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, uuid);
			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 1) {
				sqlSession.commit();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<RPackage> getPackages() {
		List<RPackage> pckgs = new ArrayList<>();
		for (AnalyzedPackage ap : analyzedPackages) {
			pckgs.add(ap.getPckg());
		}
		return pckgs;
	}
}
