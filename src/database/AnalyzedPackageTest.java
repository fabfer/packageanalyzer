package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import test.Test;
import test.TestOutput;

public class AnalyzedPackageTest {

	public enum LOG_TYPE {
		OK("ok"), NOTE("note"), WARNING("warning"), ERROR("error");
		
		private String label;
		
		private LOG_TYPE(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return label;
		}
	}
	
	public static final String FIELD_NAME_ANALYZED_PACKAGE_ID = "analyzed_package_id";
	public static final String FIELD_NAME_TEST_ID = "test_id";
	public static final String FIELD_NAME_TIME_NEEDED = "time_needed";
	public static final String FIELD_NAME_LOG = "log";
	public static final String FIELD_NAME_LOG_TYPE = "log_type";
	
	private AnalyzedPackage analyzedPackage;
	private Test test;
	private long timeNeeded;
	private String log;
	private LOG_TYPE logType;

	private AnalyzedPackageTest() {}
	
	public static AnalyzedPackageTest createInstance(SQLSession sqlSession, AnalyzedPackage analyzedPackage, Test test, TestOutput output) {
		AnalyzedPackageTest instance = new AnalyzedPackageTest();
		instance.analyzedPackage = analyzedPackage;
		instance.test = test;
		instance.timeNeeded = output.getTimeNeeded();
		instance.log = output.getLogMessage();
		instance.logType = output.getSeverity();
		if (!instance.persist(sqlSession))
			System.out.println("Unable to persist " + PackageAnalyzerDB.NAME_ANALYZED_PACKAGE_TEST + " into DB");
		return instance;
	}
	
	public static List<AnalyzedPackageTest> getAll(SQLSession sqlSession, AnalyzedPackage ap) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_ANALYZED_PACKAGE_TEST + " WHERE " + FIELD_NAME_ANALYZED_PACKAGE_ID + " = ?";
		ResultSet rs = null;
		List<AnalyzedPackageTest> pckgTests = new ArrayList<>();
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, ap.getUuid());
			rs = stmt.executeQuery();
			pckgTests = getAnalyzedPackageTests(sqlSession, rs, ap);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
		return pckgTests;
	}

	private static List<AnalyzedPackageTest> getAnalyzedPackageTests(SQLSession sqlSession, ResultSet rs, AnalyzedPackage a) throws SQLException {
		List<AnalyzedPackageTest> pckgTests = new ArrayList<>();
		try {
			while (rs != null && rs.next()) {
				if (PackageAnalyzerDB.NAME_ANALYZED_PACKAGE_TEST.equals(rs.getMetaData().getTableName(1).toLowerCase())) {
					AnalyzedPackageTest pckg = new AnalyzedPackageTest();
					pckg.analyzedPackage = a;
					pckg.test = Test.getTest(rs.getString(FIELD_NAME_TEST_ID));
					pckg.timeNeeded = rs.getLong(FIELD_NAME_TIME_NEEDED);
					pckg.log = rs.getString(FIELD_NAME_LOG);
					pckg.logType = LOG_TYPE.valueOf(rs.getString(FIELD_NAME_LOG_TYPE).toUpperCase());
					pckgTests.add(pckg);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pckgTests;
	}

	public boolean persist(SQLSession sqlSession) {
		String sql = "INSERT INTO " + PackageAnalyzerDB.NAME_ANALYZED_PACKAGE_TEST + " VALUES (?, ?, ?, ?, ?)";
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, analyzedPackage.getUuid());
			stmt.setString(index++, test.getUuid());
			stmt.setLong(index++, timeNeeded);
			stmt.setString(index++, logType.label);
			stmt.setString(index++, log);
			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 1) {
				sqlSession.commit();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public AnalyzedPackage getAnalyzedPackage() {
		return analyzedPackage;
	}

	public Test getTest() {
		return test;
	}

	public long getTimeNeeded() {
		return timeNeeded;
	}

	public String getLog() {
		return log;
	}

	public LOG_TYPE getLogType() {
		return logType;
	}

}
