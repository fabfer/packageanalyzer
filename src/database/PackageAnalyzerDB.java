package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import test.Test;

public class PackageAnalyzerDB {
	
	public static final String NAME_PACKAGE_BLUEPRINT = "package_blueprint";
	public static final String NAME_PACKAGE = "package";
	public static final String NAME_PACKAGE_DEPENDENCY = "package_dependency";
	public static final String NAME_PACKAGE_IMPORTS = "package_imports";
	public static final String NAME_PACKAGE_LINKS_TO = "package_links_to";
	public static final String NAME_PACKAGE_RECOMMENDS = "package_recommends";
	public static final String NAME_ANALYSIS = "analysis";
	public static final String NAME_ANALYZED_PACKAGE = "analyzed_package";
	public static final String NAME_ANALYZED_PACKAGE_TEST = "analyzed_package_test";
	public static final String NAME_TEST = "test";
	
	private static final String CREATE_TABLE_PACKAGE_BLUEPRINT = "CREATE TABLE " + NAME_PACKAGE_BLUEPRINT + " ("
			+ RPackageBluePrint.FIELD_NAME_UUID + " VARCHAR(36) PRIMARY KEY,"
			+ RPackageBluePrint.FIELD_NAME_NAME + " VARCHAR(100) NOT NULL)";

	private static final String CREATE_TABLE_PACKAGE = "CREATE TABLE " + NAME_PACKAGE + " ("
			+ RPackage.FIELD_NAME_UUID + " VARCHAR(36) PRIMARY KEY,"
			+ RPackage.FIELD_NAME_PAKCKAGE_BLUEPRINT_ID + " VARCHAR(36) NOT NULL,"
			+ RPackage.FIELD_NAME_VERSION + " VARCHAR(20) NOT NULL,"
			+ RPackage.FIELD_NAME_TIMESTAMP + " TIMESTAMP NOT NULL,"
			+ RPackage.FIELD_NAME_FILE_SIZE + " VARCHAR(20) NOT NULL,"
			+ RPackage.FIELD_NAME_LOCAL_FILE_URL + " VARCHAR(200) NOT NULL,"
			+ RPackage.FIELD_NAME_NEEDS_R_VERSION + " VARCHAR(20),"
			+ RPackage.FIELD_NAME_LICENSE + " VARCHAR(200),"
			+ "CONSTRAINT fk_package_package_blueprint FOREIGN KEY (" + RPackage.FIELD_NAME_PAKCKAGE_BLUEPRINT_ID + ") REFERENCES " 
				+ NAME_PACKAGE_BLUEPRINT + " (" + RPackageBluePrint.FIELD_NAME_UUID + "))";
	
	private static final String CREATE_TABLE_PACKAGE_DEPENDENCY = "CREATE TABLE " + NAME_PACKAGE_DEPENDENCY + " ("
			+ RPackage.DEPENDS_FIELD_NAME_PACKAGE_ID + " VARCHAR(36) NOT NULL,"
			+ RPackage.DEPENDS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + " VARCHAR(36) NOT NULL,"
			+ RPackage.DEPENDS_FIELD_NAME_VERSION + " VARCHAR(20),"
			+ "CONSTRAINT pk_package_dependency PRIMARY KEY (" + RPackage.DEPENDS_FIELD_NAME_PACKAGE_ID + ", " + RPackage.DEPENDS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + "),"
			+ "CONSTRAINT fk_package_dependency_package FOREIGN KEY (" + RPackage.DEPENDS_FIELD_NAME_PACKAGE_ID + ") REFERENCES "
				+ NAME_PACKAGE + " (" + RPackage.FIELD_NAME_UUID + "),"
			+ "CONSTRAINT fk_package_dependency_package_blueprint FOREIGN KEY (" + RPackage.DEPENDS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + ") REFERENCES "
				+ NAME_PACKAGE_BLUEPRINT + " (" + RPackageBluePrint.FIELD_NAME_UUID + "))";
	
	private static final String CREATE_TABLE_PACKAGE_IMPORTS = "CREATE TABLE " + NAME_PACKAGE_IMPORTS + " ("
			+ RPackage.IMPORTS_FIELD_NAME_PACKAGE_ID + " VARCHAR(36) NOT NULL,"
			+ RPackage.IMPORTS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + " VARCHAR(36) NOT NULL,"
			+ RPackage.IMPORTS_FIELD_NAME_VERSION + " VARCHAR(20),"
			+ "CONSTRAINT pk_package_imports PRIMARY KEY (" + RPackage.IMPORTS_FIELD_NAME_PACKAGE_ID + ", " + RPackage.IMPORTS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + "),"
			+ "CONSTRAINT fk_package_imports_package FOREIGN KEY (" + RPackage.IMPORTS_FIELD_NAME_PACKAGE_ID + ") REFERENCES "
				+ NAME_PACKAGE + " (" + RPackage.FIELD_NAME_UUID + "),"
			+ "CONSTRAINT fk_package_imports_package_blueprint FOREIGN KEY (" + RPackage.IMPORTS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + ") REFERENCES "
				+ NAME_PACKAGE_BLUEPRINT + " (" + RPackageBluePrint.FIELD_NAME_UUID + "))";
	
	private static final String CREATE_TABLE_PACKAGE_LINKS_TO = "CREATE TABLE " + NAME_PACKAGE_LINKS_TO + " ("
			+ RPackage.LINKS_FIELD_NAME_PACKAGE_ID + " VARCHAR(36) NOT NULL,"
			+ RPackage.LINKS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + " VARCHAR(36) NOT NULL,"
			+ RPackage.LINKS_FIELD_NAME_VERSION + " VARCHAR(20),"
			+ "CONSTRAINT pk_package_links PRIMARY KEY (" + RPackage.LINKS_FIELD_NAME_PACKAGE_ID + ", " + RPackage.LINKS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + "),"
			+ "CONSTRAINT fk_package_links_to_package FOREIGN KEY (" + RPackage.LINKS_FIELD_NAME_PACKAGE_ID + ") REFERENCES "
				+ NAME_PACKAGE + " (" + RPackage.FIELD_NAME_UUID + "),"
			+ "CONSTRAINT fk_package_links_to_package_blueprint FOREIGN KEY (" + RPackage.LINKS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + ") REFERENCES "
				+ NAME_PACKAGE_BLUEPRINT + " (" + RPackageBluePrint.FIELD_NAME_UUID + "))";

	private static final String CREATE_TABLE_PACKAGE_RECOMMENDS = "CREATE TABLE " + NAME_PACKAGE_RECOMMENDS + " ("
			+ RPackage.RECOMMENDS_FIELD_NAME_PACKAGE_ID + " VARCHAR(36) NOT NULL,"
			+ RPackage.RECOMMENDS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + " VARCHAR(36) NOT NULL,"
			+ RPackage.RECOMMENDS_FIELD_NAME_VERSION + " VARCHAR(20),"
			+ "CONSTRAINT pk_package_recommends PRIMARY KEY (" + RPackage.RECOMMENDS_FIELD_NAME_PACKAGE_ID + ", " + RPackage.RECOMMENDS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + "),"
			+ "CONSTRAINT fk_package_recommends_package FOREIGN KEY (" + RPackage.RECOMMENDS_FIELD_NAME_PACKAGE_ID + ") REFERENCES " 
				+ NAME_PACKAGE + " (" + RPackage.FIELD_NAME_UUID + "),"
			+ "CONSTRAINT fk_package_recommends_package_blueprint FOREIGN KEY (" + RPackage.RECOMMENDS_FIELD_NAME_PACKAGE_BLUEPRINT_ID + ") REFERENCES " 
				+ NAME_PACKAGE_BLUEPRINT + " (" + RPackageBluePrint.FIELD_NAME_UUID + "))";
	
	private static final String CREATE_TABLE_ANALYSIS = "CREATE TABLE " + NAME_ANALYSIS + " ("
			+ Analysis.FIELD_NAME_UUID + " VARCHAR(36) PRIMARY KEY,"
			+ Analysis.FIELD_NAME_REASON + " VARCHAR(100) NOT NULL,"
			+ Analysis.FIELD_NAME_FAST_R_VERSION + " VARCHAR(20) NOT NULL,"
			+ Analysis.FIELD_NAME_R_VERSION + " VARCHAR(20) NOT NULL,"
			+ Analysis.FIELD_NAME_DATETIME + " TIMESTAMP NOT NULL)";

	private static final String CREATE_TABLE_ANALYZED_PACKAGE = "CREATE TABLE " + NAME_ANALYZED_PACKAGE + " ("
			+ AnalyzedPackage.FIELD_NAME_UUID + " VARCHAR(36) PRIMARY KEY,"
			+ AnalyzedPackage.FIELD_NAME_ANALYSIS_ID + " VARCHAR(36) NOT NULL,"
			+ AnalyzedPackage.FIELD_NAME_PACKAGE_ID + " VARCHAR(36) NOT NULL,"
			+ "CONSTRAINT fk_analysis FOREIGN KEY (" + AnalyzedPackage.FIELD_NAME_ANALYSIS_ID + ") REFERENCES " 
				+ NAME_ANALYSIS + " (" +  Analysis.FIELD_NAME_UUID + ") ON DELETE CASCADE,"
			+ "CONSTRAINT fk_package FOREIGN KEY (" + AnalyzedPackage.FIELD_NAME_PACKAGE_ID + ") REFERENCES " 
				+ NAME_PACKAGE + " (" + RPackage.FIELD_NAME_UUID + "))";

	private static final String CREATE_TABLE_ANALYZED_PACKAGE_TEST = "CREATE TABLE " + NAME_ANALYZED_PACKAGE_TEST + " ("
			+ AnalyzedPackageTest.FIELD_NAME_ANALYZED_PACKAGE_ID + " VARCHAR(36) NOT NULL,"
			+ AnalyzedPackageTest.FIELD_NAME_TEST_ID + " VARCHAR(36) NOT NULL,"
			+ AnalyzedPackageTest.FIELD_NAME_TIME_NEEDED + " INT NOT NULL,"
			+ AnalyzedPackageTest.FIELD_NAME_LOG_TYPE + " VARCHAR(10) NOT NULL,"
			+ AnalyzedPackageTest.FIELD_NAME_LOG + " CLOB NOT NULL,"
			+ "CONSTRAINT pk_analyzed_package_test PRIMARY KEY (" 
				+ AnalyzedPackageTest.FIELD_NAME_ANALYZED_PACKAGE_ID + ", " + AnalyzedPackageTest.FIELD_NAME_TEST_ID + "),"
			+ "CONSTRAINT fk_analyzed_package FOREIGN KEY (" + AnalyzedPackageTest.FIELD_NAME_ANALYZED_PACKAGE_ID + ") REFERENCES " 
				+ NAME_ANALYZED_PACKAGE + " (" + AnalyzedPackage.FIELD_NAME_UUID + ") ON DELETE CASCADE,"
			+ "CONSTRAINT fk_test FOREIGN KEY (" + AnalyzedPackageTest.FIELD_NAME_TEST_ID + ") REFERENCES "
				+ NAME_TEST + " (" + Test.FIELD_NAME_UUID + "))";

	private static final String CREATE_TABLE_TEST = "CREATE TABLE " + NAME_TEST + " ("
			+ Test.FIELD_NAME_UUID + " VARCHAR(36) PRIMARY KEY,"
			+ Test.FIELD_NAME_DESCRIPTION + " VARCHAR(1024) NOT NULL)";

	/* functions for initializing the database */

	public static void initDatabase(SQLSession sqlSession) throws SQLException {
		if (!alreadyCreated(sqlSession)) {
			Statement stmt = sqlSession.getConnection().createStatement();
			stmt.executeUpdate(CREATE_TABLE_PACKAGE_BLUEPRINT);
			sqlSession.commit();
			stmt.executeUpdate(CREATE_TABLE_PACKAGE);
			sqlSession.commit();
			stmt.executeUpdate(CREATE_TABLE_PACKAGE_DEPENDENCY);
			sqlSession.commit();
			stmt.executeUpdate(CREATE_TABLE_PACKAGE_IMPORTS);
			sqlSession.commit();
			stmt.executeUpdate(CREATE_TABLE_PACKAGE_LINKS_TO);
			sqlSession.commit();
			stmt.executeUpdate(CREATE_TABLE_PACKAGE_RECOMMENDS);
			sqlSession.commit();
			stmt.executeUpdate(CREATE_TABLE_TEST);
			sqlSession.commit();
			stmt.executeUpdate(CREATE_TABLE_ANALYSIS);
			sqlSession.commit();
			stmt.executeUpdate(CREATE_TABLE_ANALYZED_PACKAGE);
			sqlSession.commit();
			stmt.executeUpdate(CREATE_TABLE_ANALYZED_PACKAGE_TEST);
			sqlSession.commit();
		}
	}

	private static boolean alreadyCreated(SQLSession sqlSession) {
		try (Statement stmt = sqlSession.getConnection().createStatement()) {
			stmt.execute("SELECT 1 FROM test");
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	public static ResultSet close(ResultSet rs) {
		if (rs != null)
			try {
				rs.close();
			} catch (Throwable t) {
				// ignore
			}
		return null;
	}

}
