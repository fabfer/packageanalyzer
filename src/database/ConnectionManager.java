package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	
	private static final String URL = "jdbc:derby:./package-analyzer";

	static {
		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static SQLSession beginSession() throws SQLException {
		Connection conn = null;
		try {
			while (conn == null) {
				try {
					conn = DriverManager.getConnection(URL);
				} catch (SQLException e) {
					conn = DriverManager.getConnection(URL + ";create=true");
				}
				if (!conn.isValid(5))
					conn = null;
			}
			conn.setAutoCommit(false);
			SQLSession sqlSession = new SQLSession(conn);
			PackageAnalyzerDB.initDatabase(sqlSession);
			return sqlSession;
		} catch (RuntimeException e) {
			close(conn);
			throw e;
		} catch (SQLException e) {
			close(conn);
			throw e;
		}
	}

	public static Connection close(Connection conn) {
		if (conn != null)
			try {
				conn.close();
			} catch (Throwable t) {
				// ignore
			}
		return null;
	}

}
