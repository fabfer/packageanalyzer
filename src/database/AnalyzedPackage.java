package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AnalyzedPackage {

	public static final String FIELD_NAME_UUID = "uuid";
	public static final String FIELD_NAME_ANALYSIS_ID = "analysis_id";
	public static final String FIELD_NAME_PACKAGE_ID = "package_id";

	private List<AnalyzedPackageTest> analyzedPackageTests;

	private String uuid;
	private Analysis analysis;
	private RPackage pckg;

	private AnalyzedPackage() {
		analyzedPackageTests = new ArrayList<>();
	}

	public static AnalyzedPackage getOrCreateInstance(SQLSession sqlSession, Analysis analysis, RPackage pckg) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_ANALYZED_PACKAGE + " WHERE " + FIELD_NAME_ANALYSIS_ID + " = ? AND "
				+ FIELD_NAME_PACKAGE_ID + " = ?";
		ResultSet rs = null;
		AnalyzedPackage analyzedPackage = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, analysis.getUuid());
			stmt.setString(index++, pckg.getUuid());
			rs = stmt.executeQuery();
			List<AnalyzedPackage> pckgs = getAnalyzedPackages(sqlSession, rs, analysis);
			if (pckgs.size() == 1)
				analyzedPackage = pckgs.get(0);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
		if (analyzedPackage == null) {
			analyzedPackage = new AnalyzedPackage();
			analyzedPackage.uuid = UUID.randomUUID().toString();
			analyzedPackage.analysis = analysis;
			analyzedPackage.pckg = pckg;
			if (!analyzedPackage.persist(sqlSession))
				System.out.println("Unable to persist " + PackageAnalyzerDB.NAME_ANALYZED_PACKAGE + " into DB");
		}
		return analyzedPackage;
	}

	public static List<AnalyzedPackage> getAll(SQLSession sqlSession, Analysis a) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_ANALYZED_PACKAGE + " WHERE " + FIELD_NAME_ANALYSIS_ID + " = ?";
		ResultSet rs = null;
		List<AnalyzedPackage> pckgs = new ArrayList<>();
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, a.getUuid());
			rs = stmt.executeQuery();
			pckgs = getAnalyzedPackages(sqlSession, rs, a);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
		return pckgs;
	}

	private static List<AnalyzedPackage> getAnalyzedPackages(SQLSession sqlSession, ResultSet rs, Analysis a) throws SQLException {
		List<AnalyzedPackage> pckgs = new ArrayList<>();
		try {
			while (rs != null && rs.next()) {
				if (PackageAnalyzerDB.NAME_ANALYZED_PACKAGE.equals(rs.getMetaData().getTableName(1).toLowerCase())) {
					AnalyzedPackage pckg = new AnalyzedPackage();
					pckg.uuid = rs.getString(FIELD_NAME_UUID);
					pckg.analysis = a;
					pckg.pckg = RPackage.getPackage(sqlSession, rs.getString(FIELD_NAME_PACKAGE_ID));
					pckg.analyzedPackageTests = AnalyzedPackageTest.getAll(sqlSession, pckg);
					pckgs.add(pckg);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pckgs;
	}

	public boolean persist(SQLSession sqlSession) {
		String sql = "INSERT INTO " + PackageAnalyzerDB.NAME_ANALYZED_PACKAGE + " VALUES (?, ?, ?)";
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, uuid);
			stmt.setString(index++, analysis.getUuid());
			stmt.setString(index++, pckg.getUuid());
			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 1) {
				sqlSession.commit();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<AnalyzedPackageTest> getAnalyzedPackageTests() {
		return analyzedPackageTests;
	}

	public String getUuid() {
		return uuid;
	}

	public Analysis getAnalysis() {
		return analysis;
	}

	public RPackage getPckg() {
		return pckg;
	}

	public void addAnalyzedPackageTest(AnalyzedPackageTest pckgTest) {
		analyzedPackageTests.add(pckgTest);
	}
}
