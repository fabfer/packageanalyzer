package database;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import util.RUtil;

public class RPackage {

	public static final String FIELD_NAME_UUID = "uuid";
	public static final String FIELD_NAME_PAKCKAGE_BLUEPRINT_ID = "package_blueprint_id";
	public static final String FIELD_NAME_VERSION = "version";
	public static final String FIELD_NAME_TIMESTAMP = "timestamp";
	public static final String FIELD_NAME_FILE_SIZE = "file_size";
	public static final String FIELD_NAME_LOCAL_FILE_URL = "local_file_url";
	public static final String FIELD_NAME_NEEDS_R_VERSION = "needs_r_version";
	public static final String FIELD_NAME_LICENSE = "license";

	public static final String DEPENDS_FIELD_NAME_PACKAGE_ID = "package_id";
	public static final String DEPENDS_FIELD_NAME_PACKAGE_BLUEPRINT_ID = "depends_on_package_blueprint_id";
	public static final String DEPENDS_FIELD_NAME_VERSION = "depends_on_package_version";

	public static final String IMPORTS_FIELD_NAME_PACKAGE_ID = "package_id";
	public static final String IMPORTS_FIELD_NAME_PACKAGE_BLUEPRINT_ID = "imports_package_blueprint_id";
	public static final String IMPORTS_FIELD_NAME_VERSION = "imports_package_version";

	public static final String LINKS_FIELD_NAME_PACKAGE_ID = "package_id";
	public static final String LINKS_FIELD_NAME_PACKAGE_BLUEPRINT_ID = "links_to_package_blueprint_id";
	public static final String LINKS_FIELD_NAME_VERSION = "links_to_package_version";

	public static final String RECOMMENDS_FIELD_NAME_PACKAGE_ID = "package_id";
	public static final String RECOMMENDS_FIELD_NAME_PACKAGE_BLUEPRINT_ID = "recommends_package_blueprint_id";
	public static final String RECOMMENDS_FIELD_NAME_VERSION = "recommends_package_version";

	private String uuid;
	private RPackageBluePrint blueprint;
	private String version;
	private String size;
	private Timestamp modified;
	private String localDirUrl; // Location of package folder
	private String needsRVersion;
	private String license;

	private boolean isNewPackage;

	private Map<RPackageBluePrint, String> dependsOn;
	private Map<RPackageBluePrint, String> imports;
	private Map<RPackageBluePrint, String> linksTo;
	private Map<RPackageBluePrint, String> recommends;

	private RPackage() {
		dependsOn = new HashMap<>();
		imports = new HashMap<>();
		linksTo = new HashMap<>();
		recommends = new HashMap<>();
		isNewPackage = true;
	}

	public static RPackage getOrCreateInstance(SQLSession sqlSession, String name, String version, Timestamp modified) {
		RPackageBluePrint blueprint = RPackageBluePrint.getOrCreateInstanceByName(sqlSession, name);
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_PACKAGE + " WHERE " + FIELD_NAME_PAKCKAGE_BLUEPRINT_ID + " = ? AND "
				+ FIELD_NAME_VERSION + " = ? AND " + FIELD_NAME_TIMESTAMP + " = ?";
		ResultSet rs = null;
		RPackage pckg = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, blueprint.getUuid());
			stmt.setString(index++, version);
			stmt.setTimestamp(index++, modified);
			rs = stmt.executeQuery();
			pckg = getPackage(sqlSession, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
		if (pckg == null) {
			pckg = new RPackage();
			pckg.uuid = UUID.randomUUID().toString();
			pckg.version = version;
			pckg.modified = modified;
		}
		pckg.blueprint = blueprint;
		return pckg;
	}

	public static RPackage getPackage(SQLSession sqlSession, String uuid) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_PACKAGE + " WHERE " + FIELD_NAME_UUID + " = ?";
		ResultSet rs = null;
		RPackage pckg = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, uuid);
			rs = stmt.executeQuery();
			pckg = getPackage(sqlSession, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
		return pckg;
	}

	private static RPackage getPackage(SQLSession sqlSession, ResultSet rs) throws SQLException {
		RPackage pckg = null;
		if (rs != null && rs.next()) {
			try {
				if (PackageAnalyzerDB.NAME_PACKAGE.equals(rs.getMetaData().getTableName(1).toLowerCase())) {
					pckg = new RPackage();
					pckg.uuid = rs.getString(FIELD_NAME_UUID);
					pckg.blueprint = RPackageBluePrint.getInstance(sqlSession, rs.getString(FIELD_NAME_PAKCKAGE_BLUEPRINT_ID));
					pckg.version = rs.getString(FIELD_NAME_VERSION);
					pckg.size = rs.getString(FIELD_NAME_FILE_SIZE);
					pckg.modified = rs.getTimestamp(FIELD_NAME_TIMESTAMP);
					pckg.localDirUrl = rs.getString(FIELD_NAME_LOCAL_FILE_URL);
					pckg.needsRVersion = rs.getString(FIELD_NAME_NEEDS_R_VERSION);
					pckg.license = rs.getString(FIELD_NAME_LICENSE);
					pckg.isNewPackage = false;
					initDepends(sqlSession, pckg);
					initImports(sqlSession, pckg);
					initLinks(sqlSession, pckg);
					initRecommends(sqlSession, pckg);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return pckg;
	}

	private static void initDepends(SQLSession sqlSession, RPackage pckg) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_PACKAGE_DEPENDENCY + " WHERE " + DEPENDS_FIELD_NAME_PACKAGE_ID
				+ " = ?";
		ResultSet rs = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			stmt.setString(1, pckg.uuid);
			rs = stmt.executeQuery();
			while (rs != null && rs.next()) {
				String blueprintId = rs.getString(DEPENDS_FIELD_NAME_PACKAGE_BLUEPRINT_ID);
				String version = rs.getString(DEPENDS_FIELD_NAME_VERSION);
				pckg.addDependsOn(RPackageBluePrint.getInstance(sqlSession, blueprintId), version);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
	}

	private static void initImports(SQLSession sqlSession, RPackage pckg) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_PACKAGE_IMPORTS + " WHERE " + IMPORTS_FIELD_NAME_PACKAGE_ID + " = ?";
		ResultSet rs = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			stmt.setString(1, pckg.uuid);
			rs = stmt.executeQuery();
			while (rs != null && rs.next()) {
				String blueprintId = rs.getString(IMPORTS_FIELD_NAME_PACKAGE_BLUEPRINT_ID);
				String version = rs.getString(IMPORTS_FIELD_NAME_VERSION);
				pckg.addImports(RPackageBluePrint.getInstance(sqlSession, blueprintId), version);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
	}

	private static void initLinks(SQLSession sqlSession, RPackage pckg) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_PACKAGE_LINKS_TO + " WHERE " + LINKS_FIELD_NAME_PACKAGE_ID + " = ?";
		ResultSet rs = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			stmt.setString(1, pckg.uuid);
			rs = stmt.executeQuery();
			while (rs != null && rs.next()) {
				String blueprintId = rs.getString(LINKS_FIELD_NAME_PACKAGE_BLUEPRINT_ID);
				String version = rs.getString(LINKS_FIELD_NAME_VERSION);
				pckg.addLinksTo(RPackageBluePrint.getInstance(sqlSession, blueprintId), version);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
	}

	private static void initRecommends(SQLSession sqlSession, RPackage pckg) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_PACKAGE_RECOMMENDS + " WHERE " + RECOMMENDS_FIELD_NAME_PACKAGE_ID
				+ " = ?";
		ResultSet rs = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			stmt.setString(1, pckg.uuid);
			rs = stmt.executeQuery();
			while (rs != null && rs.next()) {
				String blueprintId = rs.getString(RECOMMENDS_FIELD_NAME_PACKAGE_BLUEPRINT_ID);
				String version = rs.getString(RECOMMENDS_FIELD_NAME_VERSION);
				pckg.addRecommends(RPackageBluePrint.getInstance(sqlSession, blueprintId), version);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
	}

	public void addDependsOn(RPackageBluePrint blueprint, String version) {
		dependsOn.put(blueprint, version);
	}

	public void addImports(RPackageBluePrint blueprint, String version) {
		imports.put(blueprint, version);
	}

	public void addLinksTo(RPackageBluePrint blueprint, String version) {
		linksTo.put(blueprint, version);
	}

	public void addRecommends(RPackageBluePrint blueprint, String version) {
		recommends.put(blueprint, version);
	}

	public String getUuid() {
		return uuid;
	}

	public RPackageBluePrint getBlueprint() {
		return blueprint;
	}

	public String getVersion() {
		return version;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Date getModified() {
		return modified;
	}

	public String getLocalDirUrl() {
		return localDirUrl;
	}

	public void setLocalDirUrl(String localDirUrl) {
		this.localDirUrl = localDirUrl;
	}

	public String getNeedsRVersion() {
		return needsRVersion;
	}

	public void setNeedsRVersion(String needsRVersion) {
		this.needsRVersion = needsRVersion;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public boolean isNewPackage() {
		return isNewPackage;
	}

	public Map<RPackageBluePrint, String> getDependsOn() {
		return dependsOn;
	}

	public Map<RPackageBluePrint, String> getImports() {
		return imports;
	}

	public boolean persist(SQLSession sqlSession) {
		String sql = "INSERT INTO " + PackageAnalyzerDB.NAME_PACKAGE + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, uuid);
			stmt.setString(index++, blueprint.getUuid());
			stmt.setString(index++, version);
			stmt.setTimestamp(index++, modified);
			stmt.setString(index++, size);
			stmt.setString(index++, localDirUrl);
			stmt.setString(index++, needsRVersion);
			stmt.setString(index++, license);
			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 1) {
				sqlSession.commit();
				// insert dependencies
				insertRelation(sqlSession, PackageAnalyzerDB.NAME_PACKAGE_DEPENDENCY, dependsOn);
				// insert imports
				insertRelation(sqlSession, PackageAnalyzerDB.NAME_PACKAGE_IMPORTS, imports);
				// insert links
				insertRelation(sqlSession, PackageAnalyzerDB.NAME_PACKAGE_LINKS_TO, linksTo);
				// insert recommendations
				insertRelation(sqlSession, PackageAnalyzerDB.NAME_PACKAGE_RECOMMENDS, recommends);
				sqlSession.commit();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private void insertRelation(SQLSession sqlSession, String tableName, Map<RPackageBluePrint, String> map) throws SQLException {
		int index;
		String sql = "INSERT INTO " + tableName + " VALUES (?, ?, ?)";
		PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql);
		for (Entry<RPackageBluePrint, String> entry : map.entrySet()) {
			index = 1;
			stmt.setString(index++, uuid);
			stmt.setString(index++, entry.getKey().getUuid());
			stmt.setString(index++, entry.getValue());
			stmt.executeUpdate();
		}
	}

	public boolean existsLocally() {
		File folder = new File("resources/packages/" + localDirUrl);
		return folder.exists() && folder.isDirectory();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((blueprint == null) ? 0 : blueprint.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RPackage other = (RPackage) obj;
		if (blueprint == null) {
			if (other.blueprint != null)
				return false;
		} else if (!blueprint.equals(other.blueprint))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return blueprint.toString() + " " + version;
	}

	public boolean needsNewRVersion(String rVersion) {
		return needsRVersion != null && needsRVersion.compareTo(rVersion) > 0;
	}

	public boolean onlyDependsOn(List<RPackage> to) {
		for (Entry<RPackageBluePrint, String> entry : getDependsOn().entrySet()) {
			boolean found = RUtil.fastrBasePackages.contains(entry.getKey().getName());
			if (!found) {
				for (RPackage toPckg : to) {
					if (toPckg.getBlueprint().equals(entry.getKey())) {
						found = true;
						break;
					}
				}
				if (!found) {
					return false;
				}
			}
		}
		for (Entry<RPackageBluePrint, String> entry : getImports().entrySet()) {
			boolean found = RUtil.fastrBasePackages.contains(entry.getKey().getName());
			if (!found) {
				for (RPackage toPckg : to) {
					if (toPckg.getBlueprint().equals(entry.getKey())) {
						found = true;
						break;
					}
				}
				if (!found) {
					return false;
				}
			}
		}
		return true;
	}

	public static RPackage getPackage(SQLSession sqlSession, String blueprintUuid, String version) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_PACKAGE + " WHERE " + FIELD_NAME_PAKCKAGE_BLUEPRINT_ID + " = ? "
				+ (version != null ? "AND version >= ? " : "") + "ORDER BY " + FIELD_NAME_VERSION + " desc FETCH FIRST ROW ONLY";
		ResultSet rs = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, blueprintUuid);
			if (version != null)
				stmt.setString(index++, version);
			rs = stmt.executeQuery();
			return getPackage(sqlSession, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
		return null;
	}

	public static RPackage getPackageByName(String name, Date d, SQLSession sqlSession) {
		RPackage result = null;
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_PACKAGE + " p INNER JOIN " + PackageAnalyzerDB.NAME_PACKAGE_BLUEPRINT
				+ " pb ON p." + FIELD_NAME_PAKCKAGE_BLUEPRINT_ID + " = pb." + RPackageBluePrint.FIELD_NAME_UUID + " WHERE pb."
				+ RPackageBluePrint.FIELD_NAME_NAME + " = ? ";
		if (d != null)
			sql += "AND p." + FIELD_NAME_TIMESTAMP + " < ? ";
		sql += "ORDER BY " + FIELD_NAME_TIMESTAMP + " DESC FETCH FIRST ROW ONLY";
		ResultSet rs = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, name);
			if (d != null)
				stmt.setTimestamp(index++, new Timestamp(d.getTime()));
			rs = stmt.executeQuery();
			result = getPackage(sqlSession, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
		return result;
	}
}
