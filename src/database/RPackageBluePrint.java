package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class RPackageBluePrint {

	public static final String FIELD_NAME_UUID = "uuid";
	public static final String FIELD_NAME_NAME = "name";
	
	private String uuid;
	private String name;
	
	private RPackageBluePrint() {}
	
	public static RPackageBluePrint getInstance(SQLSession sqlSession, String uuid) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_PACKAGE_BLUEPRINT + " WHERE " + FIELD_NAME_UUID + " = ?";
		ResultSet rs = null;
		RPackageBluePrint blueprint = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			stmt.setString(1, uuid);
			rs = stmt.executeQuery();
			blueprint = getPackageBlueprint(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
		return blueprint;
	}
	
	public static RPackageBluePrint getOrCreateInstanceByName(SQLSession sqlSession, String name) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_PACKAGE_BLUEPRINT + " WHERE " + FIELD_NAME_NAME + " = ?";
		ResultSet rs = null;
		RPackageBluePrint blueprint = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			stmt.setString(1, name);
			rs = stmt.executeQuery();
			blueprint = getPackageBlueprint(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
		if (blueprint == null) {
			blueprint = new RPackageBluePrint();
			blueprint.uuid = UUID.randomUUID().toString();
			blueprint.name = name;
			if (!blueprint.persist(sqlSession))
				System.out.println("Unable to persist " + PackageAnalyzerDB.NAME_PACKAGE_BLUEPRINT + " into DB");
		}
		return blueprint;
	}
	
	private boolean persist(SQLSession sqlSession) {
		String sql = "INSERT INTO " + PackageAnalyzerDB.NAME_PACKAGE_BLUEPRINT + " VALUES (?, ?)";
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, uuid);
			stmt.setString(index++, name);
			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 1) {
				sqlSession.commit();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private static RPackageBluePrint getPackageBlueprint(ResultSet rs) throws SQLException {
		RPackageBluePrint packageBlueprint = null;
		if (rs != null && rs.next()) {
			try {
				if (PackageAnalyzerDB.NAME_PACKAGE_BLUEPRINT.equals(rs.getMetaData().getTableName(1).toLowerCase())) {
					packageBlueprint = new RPackageBluePrint();
					packageBlueprint.setUuid(rs.getString(FIELD_NAME_UUID));
					packageBlueprint.setName(rs.getString(FIELD_NAME_NAME));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return packageBlueprint;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RPackageBluePrint other = (RPackageBluePrint) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public RPackage findPackage(List<RPackage> packages) {
		for (RPackage pckg : packages) {
			if (pckg.getBlueprint().equals(this))
				return pckg;
		}
		return null;
	}
}
