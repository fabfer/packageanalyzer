package database;

import java.sql.Connection;
import java.sql.SQLException;

public class SQLSession implements AutoCloseable {

	private Connection connection;

	public SQLSession(Connection connection) {
		this.connection = connection;
	}

	public Connection getConnection() {
		return connection;
	}

	public void commit() throws SQLException {
		if (!connection.getAutoCommit())
			connection.commit();
	}

	public void rollback() throws SQLException {
		if (!connection.isClosed())
			connection.rollback();
	}

	public void close() {
		try {
			rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		connection = ConnectionManager.close(connection);
	}

}
