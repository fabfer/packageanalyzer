package output;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;

import database.Analysis;
import database.AnalyzedPackage;
import database.AnalyzedPackageTest;
import database.AnalyzedPackageTest.LOG_TYPE;
import test.Test;

public class PackageAnalyzerHTMLPage {

	private static final SimpleDateFormat STORAGE_FORMAT = new SimpleDateFormat("yyyy-MM-dd-HH-mm", Locale.GERMAN);
	private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd MMM. yyyy HH:mm", Locale.GERMAN);

	private Analysis newAnalysis;
	private Analysis[] oldAnalysis;

	public PackageAnalyzerHTMLPage(Analysis newAnalysis, Analysis... oldAnalysis) {
		this.newAnalysis = newAnalysis;
		this.oldAnalysis = oldAnalysis;
	}

	public PackageAnalyzerHTMLPage(List<Analysis> oldAnalysis) {
		this.newAnalysis = oldAnalysis.get(0);
		this.oldAnalysis = oldAnalysis.subList(1, oldAnalysis.size()).toArray(new Analysis[0]);
	}

	public void buildAndShow() throws IOException {
		File file = new File("resources/analyses/analysis" + STORAGE_FORMAT.format(newAnalysis.getDatetime()) + ".html");
		file.getParentFile().mkdirs();
		PrintWriter writer = new PrintWriter(file.getAbsolutePath(), "UTF-8");
		writer.println("<html>");
		writer.println("<head>");
		writer.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\" />");
		writer.println("<script src=\"scripts/jquery-2.1.4.min.js\"></script>");
		writer.println("<script src=\"scripts/analysis.js\"></script>");

		printElementDescriptions(writer);

		writer.println("</head>");
		writer.println("<body>");

		printBasicInformation(writer);

		writer.println("<table id=\"main-table\">");
		writer.println("<tbody>");
		writer.println("<tr>");
		writer.println("<td rowspan=\"2\" id=\"test-navigation\">");
		writer.println("<ul id=\"tests\">");
		printTestList(writer, Test.ROOT_TESTS, 0);
		writer.println("</ul>");
		writer.println("</td>");
		writer.println("<td style=\"padding: 0 10px\">");
		printFilter(writer);
		writer.println("</td>");
		writer.println("</tr>");
		writer.println("<tr>");
		writer.println("<td style=\"padding: 0 10px\">");
		printContent(writer);
		writer.println("</td>");
		writer.println("</tr>");
		writer.println("</tbody>");
		writer.println("</table>");

		writer.println("</body>");
		writer.println("</html>");
		writer.close();
		Desktop.getDesktop().browse(file.toURI());
	}

	private void printElementDescriptions(PrintWriter writer) {
		writer.println("<script>");
		writer.println("var packages = {};");
		Set<String> packageNames = new HashSet<>();
		for (AnalyzedPackage analyzedPackage : newAnalysis.getAnalyzedPackages()) {
			for (AnalyzedPackageTest pckgTest : analyzedPackage.getAnalyzedPackageTests()) {
				String name = StringEscapeUtils.escapeEcmaScript(pckgTest.getAnalyzedPackage().getPckg().getBlueprint().getName());
				if (packageNames.add(name)) {
					writer.print("packages['");
					writer.print(name);
					writer.print("'] = {};");
					writer.print("packages['");
					writer.print(name);
					writer.print("']['version'] = '");
					writer.print(pckgTest.getAnalyzedPackage().getPckg().getVersion());
					writer.println("';");
				}
				generateJavascriptPackageTest(writer, pckgTest);
			}
		}
		writer.println("</script>");
	}

	private void generateJavascriptPackageTest(PrintWriter writer, AnalyzedPackageTest pckgTest) {
		writer.print("packages['");
		writer.print(StringEscapeUtils.escapeEcmaScript(pckgTest.getAnalyzedPackage().getPckg().getBlueprint().getName()));
		writer.print("']['");
		writer.print(pckgTest.getTest().getUuid());
		writer.print("'] = {");

		writer.print("logtype:'");
		writer.print(pckgTest.getLogType().name());
		writer.print("',time:'");
		writer.print(pckgTest.getTimeNeeded() / 1000.0f + " s");
		writer.print("',log:'");
		writer.print(StringEscapeUtils.escapeEcmaScript(pckgTest.getLog()));
		writer.print("'");

		boolean first = true;
		for (Analysis old : oldAnalysis) {
			AnalyzedPackageTest oldTest = find(old.getAnalyzedPackages(),
					pckgTest.getAnalyzedPackage().getPckg().getBlueprint().getUuid(), pckgTest.getTest().getUuid());
			if (oldTest != null && resultsDiffer(pckgTest, oldTest)) {
				if (first) {
					writer.print(",old:{");
					first = false;
				} else
					writer.print(",");
				writer.print("'");
				writer.print(oldTest.getAnalyzedPackage().getPckg().getVersion());
				writer.print("':{");

				if (!oldTest.getLogType().name().equals(pckgTest.getLogType().name())) {
					writer.print("logtype:'");
					writer.print(StringEscapeUtils.escapeEcmaScript(oldTest.getLogType().name()));
					writer.print("',");
				}
				writer.print("time:'");
				writer.print(oldTest.getTimeNeeded() / 1000.0f + " s");
				writer.print("'");
				if (!oldTest.getLog().equals(pckgTest.getLog())) {
					writer.print(",log:'");
					writer.print(StringEscapeUtils.escapeEcmaScript(oldTest.getLog()));
					writer.print("'");
				}

				writer.print("}");
			}
		}
		if (!first)
			writer.print("}");

		writer.println("}");
	}

	private void printFilter(PrintWriter writer) {
		writer.println("<div class=\"box\">");
		writer.println("<div class=\"container-1\">");
		writer.println("<img class=\"icon\" src=\"img/ic_search.png\" />");
		writer.println("<input type=\"text\" id=\"search\" placeholder=\"Search anything...\" onkeyup=\"updateSearch(this)\"/>");
		writer.println("</div>");
		writer.println("</div>");
		if (oldAnalysis != null && oldAnalysis.length >= 1) {
			writer.println("<div>");
			writer.println("<input type=\"checkbox\" name=\"onlyDiff\" id=\"onlyDiff\" onclick=\"startUpdate()\"/>");
			writer.println("<label for=\"onlyDiff\">Show only packages with different results</label>");
			writer.println("</div>");
			writer.println("<div>");
			writer.println("<input type=\"checkbox\" name=\"onlyNew\" id=\"onlyNew\" onclick=\"startUpdate()\"/>");
			writer.println("<label for=\"onlyNew\">Show only new packages (that did not exist in the old analysis)</label>");
			writer.println("</div>");
		}
		writer.print("<div id=\"log-types\">");
		writer.print("<span>Log-types</span>");
		for (LOG_TYPE type : LOG_TYPE.values()) {
			writer.print("<div id=\"");
			writer.print(type.name());
			writer.print("\" class=\"log-type ");
			writer.print(type.name());
			writer.print("\" onclick=\"filterResult(this);startUpdate();\">");
			writer.print("<span class=\"count\">0</span>");
			writer.print(type.name());
			writer.print("</div>");
		}
		writer.println("</div>");
	}

	private void printTestList(PrintWriter writer, List<Test> tests, int level) {
		for (Test t : tests) {
			writer.print("<li id=\"");
			writer.print(t.getUuid());
			writer.print("\" class=\"level");
			writer.print(level);
			writer.print("\" onclick=\"filterTest(this);startUpdate();\">");
			writer.print("<span class=\"count\">0</span>");
			writer.print(StringEscapeUtils.escapeHtml4(t.getTestDescription()));
			writer.println("</li>");
			printTestList(writer, t.getDependingTests(), level + 1);
		}
	}

	private void printBasicInformation(PrintWriter writer) {
		writer.print("<div>R version: " + newAnalysis.getRVersion());
		int i = 0;
		for (Analysis old : oldAnalysis) {
			writer.print(" / <span class=\"old" + i++ + "\">" + old.getRVersion() + "</span>");
		}
		writer.println("</div>");
		writer.print("<div>FastR version: " + newAnalysis.getFastRVersion());
		i = 0;
		for (Analysis old : oldAnalysis) {
			writer.print(" / <span class=\"old" + i++ + "\">" + old.getFastRVersion() + "</span>");
		}
		writer.println("</div>");
		writer.print("<div>Analyzed: " + DATE_TIME_FORMAT.format(newAnalysis.getDatetime()));
		i = 0;
		for (Analysis old : oldAnalysis) {
			writer.print(" / <span class=\"old" + i++ + "\">" + DATE_TIME_FORMAT.format(old.getDatetime()) + "</span>");
		}
		writer.print("<div>Reason: " + StringEscapeUtils.escapeHtml4(newAnalysis.getReason()));
		i = 0;
		for (Analysis old : oldAnalysis) {
			writer.print(" / <span class=\"old" + i++ + "\">" + StringEscapeUtils.escapeHtml4(old.getReason()) + "</span>");
		}
		writer.println("</div>");
	}

	private void printContent(PrintWriter writer) {
		// gets filled dynamically
		writer.println("<div id=\"content\">");
		writer.println("</div>");
	}

	private boolean resultsDiffer(AnalyzedPackageTest pckgTest, AnalyzedPackageTest oldTest) {
		return !pckgTest.getLog().equals(oldTest) || !pckgTest.getLogType().equals(oldTest.getLogType());
	}

	/*
	 * Find an old analyzed package by matching package id and test id
	 */
	private AnalyzedPackageTest find(List<AnalyzedPackage> analyzedPackages, String packageBlueprintId, String testId) {
		for (AnalyzedPackage pckg : analyzedPackages) {
			if (pckg.getPckg().getBlueprint().getUuid().equals(packageBlueprintId)) {
				for (AnalyzedPackageTest test : pckg.getAnalyzedPackageTests()) {
					if (test.getTest().getUuid().equals(testId))
						return test;
				}
			}
		}
		return null;
	}

}
