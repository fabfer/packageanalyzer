package util;

public class CmdOutput {

	public enum OUTPUT_TYPE {
		OK, ERROR
	}

	private String output;
	private OUTPUT_TYPE type;

	public CmdOutput(String output, OUTPUT_TYPE type) {
		this.output = output;
		this.type = type;
	}

	public String getOutput() {
		return output;
	}

	public OUTPUT_TYPE getType() {
		return type;
	}

	public boolean wasInstalledSuccessfully(String packageName) {
		String tmpOutput = TextUtil.trimToNull(output);
		return tmpOutput != null && tmpOutput.contains("* DONE (" + packageName + ")");
	}
}
