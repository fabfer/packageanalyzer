package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;

import database.RPackage;
import database.RPackageBluePrint;
import database.SQLSession;
import main.PackageAnalyzer;
import util.CmdOutput.OUTPUT_TYPE;

public class RUtil {
	
	public static final List<String> fastrBasePackages = new ArrayList<>();
	public static final List<String> rBasePackages = new ArrayList<>();
	// Some packages are needed for information extraction
	// These packages must be handled separately in the programs scope
	public static final List<String> neededRPackages = new ArrayList<>();
	public static String R_VERSION = "no version found";
	public static String FAST_R_VERSION = "no version found";

	public static final String FASTRSCRIPT_CMD = "fastrscript";
	public static final String RSCRIPT_CMD = "custrscript";

	public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.ENGLISH);
	public static final SimpleDateFormat DB_TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
	public static final SimpleDateFormat FILE_NAME_TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd_HH-mm", Locale.ENGLISH);

	public static final boolean REDIRECT = true;

	private static Pattern valuePattern = Pattern.compile("\"([^\"]*)\"");

	public static final String R_LIB = "resources/lib_gnur/";
	public static final String FASTR_LIB = "resources/lib_fastr/";
	
	public static final String PACKAGE_LOCATION = "resources/packages/";
	public static final String SCRIPTS_LOCATION = "resources/scripts/";
	public static final String SCRIPT_INSTALL_LOCAL_PACKAGE = "install_local_package.R";

	private static final String MIRROR = "http://cran.r-project.org/src/contrib/";
	private static final String SCRIPT_PACKAGE_DATE_AND_SIZE = "package_details.R";
	private static final String SCRIPT_PACKAGE_RELATIONS = "available_packages.R";
	private static final String SCRIPT_UNINSTALL_PACKAGE = "uninstall_package.R";
	private static final String SCRIPT_CHECK_IF_INSTALLED = "check_if_installed.R";
	private static final String SCRIPT_BASE_PACKAGES = "base_packages.R";
	private static final String SCRIPT_VERSION_INFO = "start_output.R";

	private enum PARSE_STATE {
		LICENSE("License"), DEPENDENCY("Depends"), IMPORTS("Imports"), LINK_TO("LinkingTo"), RECOMMENDS("Suggests"), NONE("");

		private String keywords;

		private PARSE_STATE(String contains) {
			this.keywords = contains;
		}

		static PARSE_STATE indicatesNewState(String line) {
			PARSE_STATE state = NONE;
			if (LICENSE.keywords.equals(line.trim()))
				state = LICENSE;
			else if (IMPORTS.keywords.equals(line.trim()))
				state = IMPORTS;
			else if (DEPENDENCY.keywords.equals(line.trim()))
				state = DEPENDENCY;
			else if (LINK_TO.keywords.equals(line.trim()))
				state = LINK_TO;
			else if (RECOMMENDS.keywords.equals(line.trim()))
				state = RECOMMENDS;
			return state;
		}
	}

	/** PUBLIC methods */

	public static void loadPackages(SQLSession sqlSession) throws IOException, InterruptedException {
		System.out.print("--> GETTING package information");
		String packageInfo = getInfoFromMirror(SCRIPT_PACKAGE_DATE_AND_SIZE).getOutput();
		System.out.println(" [done]");
		System.out.print("--> GETTING package relations");
		String packageDetails = getInfoFromMirror(SCRIPT_PACKAGE_RELATIONS).getOutput();
		System.out.println(" [done]");
		System.out.println("--> PARSING package information");
		String[] lines = packageInfo.split("\\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			TextUtil.updateProgress((i + 1) / (double) lines.length);
			String[] parts = line.split("\\s+"); // id name date time size
			if (parts.length == 5) {
				if (parts[1].split("_").length == 2) {
					String name = parts[1].split("_")[0];
					String version = parts[1].split("_")[1].replace(".tar.gz", "");
					Timestamp dateTime;
					try {
						dateTime = new Timestamp(DATE_TIME_FORMAT.parse(parts[2] + " " + parts[3]).getTime());
					} catch (ParseException e) {
						dateTime = new Timestamp(new Date().getTime());
						e.printStackTrace();
					}
					RPackage pckg = RPackage.getOrCreateInstance(sqlSession, name, version, dateTime);
					if (pckg.isNewPackage()) {
						PackageAnalyzer.newPackages.add(pckg);
						pckg.setSize(parts[4]);
						pckg.setLocalDirUrl(name + "_" + version + "_" + FILE_NAME_TIMESTAMP_FORMAT.format(dateTime));
					} else {
						PackageAnalyzer.oldPackages.add(pckg);
					}
				}
			}
		}

		System.out.println("");
		if (PackageAnalyzer.newPackages.size() >= 0) {
			System.out.println("--> PARSING relations for new packages");
			PARSE_STATE state = PARSE_STATE.NONE;
			// remove empty lines
			packageDetails = packageDetails.replaceAll("(?m)^\\s+$", "");

			lines = packageDetails.split("\\n");
			for (int i = 0; i < lines.length; i++) {
				String line = lines[i];
				TextUtil.updateProgress((i + 1) / (double) lines.length);
				PARSE_STATE newState = PARSE_STATE.indicatesNewState(line);
				if (newState != PARSE_STATE.NONE || line.startsWith(" ")) {
					state = newState;
				} else if (state != PARSE_STATE.NONE) {
					String name = line.split("\\s+")[0];
					Matcher matcher = valuePattern.matcher(line);
					String value = null;
					if (matcher.find())
						value = matcher.group(0);
					RPackage pckg = getNewPackageByName(name);
					if (pckg != null && value != null) {
						value = TextUtil.removeBackslashN(TextUtil.removeApostrophes(value));
						if (state == PARSE_STATE.LICENSE) {
							pckg.setLicense(value);
						} else if (state == PARSE_STATE.DEPENDENCY) {
							Map<String, String> relations = TextUtil.getRelations(value);
							pckg.setNeedsRVersion(relations.get("R"));
							relations.remove("R");
							for (Entry<String, String> relation : relations.entrySet()) {
								pckg.addDependsOn(RPackageBluePrint.getOrCreateInstanceByName(sqlSession, relation.getKey()),
										relation.getValue());
							}
						} else if (state == PARSE_STATE.IMPORTS) {
							Map<String, String> relations = TextUtil.getRelations(value);
							for (Entry<String, String> relation : relations.entrySet()) {
								pckg.addImports(RPackageBluePrint.getOrCreateInstanceByName(sqlSession, relation.getKey()),
										relation.getValue());
							}
						} else if (state == PARSE_STATE.LINK_TO) {
							Map<String, String> relations = TextUtil.getRelations(value);
							for (Entry<String, String> relation : relations.entrySet()) {
								pckg.addLinksTo(RPackageBluePrint.getOrCreateInstanceByName(sqlSession, relation.getKey()),
										relation.getValue());
							}
						} else if (state == PARSE_STATE.RECOMMENDS) {
							Map<String, String> relations = TextUtil.getRelations(value);
							for (Entry<String, String> relation : relations.entrySet()) {
								pckg.addRecommends(RPackageBluePrint.getOrCreateInstanceByName(sqlSession, relation.getKey()),
										relation.getValue());
							}
						}
					}
				}
			}
			System.out.println("");
			// now persist new packages in the DB
			System.out.println("--> STORING new packages in DB");
			for (RPackage pckg : PackageAnalyzer.newPackages) {
				pckg.persist(sqlSession);
			}
		} else {
			System.out.println("--> No new packages found");
		}
	}

	public static CmdOutput execCmdWithDir(String dir, boolean redirect, String... args) throws IOException, InterruptedException {
		ProcessBuilder pBuilder = new ProcessBuilder(args);
		pBuilder.redirectErrorStream(redirect);
		if (dir != null)
			pBuilder.directory(new File(dir));
		StringBuilder output = new StringBuilder();
		Process proc = pBuilder.start();
		BufferedReader input = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		String line = null;
		while ((line = input.readLine()) != null) {
			output.append(line + "\n");
		}

		CmdOutput result = new CmdOutput(output.toString(), OUTPUT_TYPE.OK);

		if (output.length() == 0) {
			BufferedReader errorInput = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
			while ((line = errorInput.readLine()) != null) {
				output.append(line + "\n");
			}
			result = new CmdOutput(output.toString(), OUTPUT_TYPE.ERROR);
		}

		proc.waitFor();
		return result;
	}

	public static CmdOutput execCmd(String... args) throws IOException, InterruptedException {
		return execCmd(false, args);
	}

	public static CmdOutput execCmd(String cmd, File script, String... args) throws IOException, InterruptedException {
		return execCmd(false, cmd, script, args);
	}

	public static CmdOutput execCmd(boolean redirect, String... args) throws IOException, InterruptedException {
		return execCmdWithDir(null, redirect, args);
	}

	public static CmdOutput execCmd(boolean redirect, String cmd, File script, String... args)
			throws IOException, InterruptedException {
		if (script.exists() && script.isFile()) {
			List<String> allArgs = new ArrayList<>();
			allArgs.add(cmd);
			allArgs.add(script.getAbsolutePath());
			allArgs.addAll(Arrays.asList(args));
			return execCmd(redirect, allArgs.toArray(new String[0]));
		} else
			throw new IOException("File " + script.getName() + " does not exist");
	}

	public static void loadVersions() throws Exception {
		try {
			CmdOutput output = execCmd(RSCRIPT_CMD, SCRIPTS_LOCATION + SCRIPT_VERSION_INFO);
			if (output.getType() == OUTPUT_TYPE.OK) {
				String major = getMajor(output.getOutput());
				String minor = getMinor(output.getOutput());
				RUtil.R_VERSION = major + "." + minor;
			} else
				throw new IOException(output.getOutput());
		} catch (IOException | InterruptedException e) {
			throw new Exception("Could not detect current R version: " + e.getMessage());
		}

		try {
			CmdOutput output = execCmd(FASTRSCRIPT_CMD, new File(SCRIPTS_LOCATION + SCRIPT_VERSION_INFO));
			if (output.getType() == OUTPUT_TYPE.OK) {
				String major = getMajor(output.getOutput());
				String minor = getMinor(output.getOutput());
				RUtil.FAST_R_VERSION = major + "." + minor;
			} else
				throw new IOException(output.getOutput());
		} catch (IOException | InterruptedException e) {
			throw new Exception("Could not detect current FastR version: " + e.getMessage());
		}
	}

	public static void downloadPackage(RPackage pckg)
			throws MalformedURLException, FileNotFoundException, IOException, InterruptedException {
		String zipFileName = pckg.getBlueprint().getName() + "_" + pckg.getVersion() + ".tar.gz";
		IOUtils.copy(new URL(MIRROR + zipFileName).openStream(), new FileOutputStream(PACKAGE_LOCATION + zipFileName));
		execCmdWithDir(PACKAGE_LOCATION, false, "tar", "-zxf", zipFileName);
		execCmdWithDir(PACKAGE_LOCATION, false, "rm", "-rf", zipFileName);
		execCmdWithDir(PACKAGE_LOCATION, false, "mv", pckg.getBlueprint().getName(), pckg.getLocalDirUrl());
	}

	public static void checkIfInstalled(String program) throws Exception {
		try {
			CmdOutput output = execCmd(program, new File(SCRIPTS_LOCATION + SCRIPT_VERSION_INFO)); // deliver any script
			if (output.getType() == OUTPUT_TYPE.ERROR)
				throw new IOException(output.getOutput());
		} catch (IOException | InterruptedException e) {
			throw new Exception("Executable " + program + " probably not found in PATH (" + System.getenv("PATH") + ")\n" + e.getMessage());
		}
	}

	public static List<String> getBasePackages(String program) throws Exception {
		try {
			CmdOutput output = execCmd(program, new File(SCRIPTS_LOCATION + SCRIPT_BASE_PACKAGES));
			if (output.getType() == OUTPUT_TYPE.OK) {
				List<String> basePackages = new ArrayList<>();
				String[] parts = output.getOutput().split("\\s+");
				for (String part : parts) {
					part = TextUtil.removeApostrophes(part);
					if (part != null && !basePackages.contains(part))
						basePackages.add(part);
				}
				return basePackages;
			} else
				throw new IOException();
		} catch (IOException | InterruptedException e) {
			throw new Exception("Base packages of " + program + " could not be identified!");
		}
	}

	public static void checkIfPackageIsInstalled(String program, String pckg) throws Exception {
		try {
			CmdOutput output = execCmd(program, new File(SCRIPTS_LOCATION + SCRIPT_CHECK_IF_INSTALLED), pckg);
			if (output.getType() != OUTPUT_TYPE.OK || TextUtil.trimToNull(output.getOutput()) == null)
				throw new IOException();
		} catch (IOException | InterruptedException e) {
			throw new Exception("Required package " + pckg + " is not installed in " + program + "");
		}
	}

	public static CmdOutput uninstallPackage(String program, RPackage pckg) throws IOException, InterruptedException {
		return execCmd(program, new File(SCRIPTS_LOCATION + SCRIPT_UNINSTALL_PACKAGE), pckg.getBlueprint().getName());
	}

	/** PRIVATE methods */

	private static CmdOutput getInfoFromMirror(String fileName) throws IOException, InterruptedException {
		return execCmd(RSCRIPT_CMD, SCRIPTS_LOCATION + fileName, MIRROR);
	}

	private static String getMinor(String output) throws IOException {
		for (String line : output.split("\\n")) {
			if (line.startsWith("minor")) {
				return line.split("\\s+")[1];
			}
		}
		throw new IOException("No minor version found in: " + output);
	}

	private static String getMajor(String output) throws IOException {
		for (String line : output.split("\\n")) {
			if (line.startsWith("major")) {
				return line.split("\\s+")[1];
			}
		}
		throw new IOException("No major version found in: " + output);
	}

	private static RPackage getNewPackageByName(String name) {
		for (RPackage pckg : PackageAnalyzer.newPackages) {
			if (pckg.getBlueprint().getName().equals(name))
				return pckg;
		}
		return null;
	}

	public static CmdOutput installLocalPackage(String cmd, String pckg) throws IOException, InterruptedException {
		File pckgLocation = new File(RUtil.PACKAGE_LOCATION + pckg);
		return execCmd(RUtil.REDIRECT, cmd, new File(RUtil.SCRIPTS_LOCATION + RUtil.SCRIPT_INSTALL_LOCAL_PACKAGE), pckgLocation.getAbsolutePath());
	}

}
