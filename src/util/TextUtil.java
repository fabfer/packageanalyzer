package util;

import java.util.HashMap;
import java.util.Map;

public class TextUtil {

	public static String trim(String str) {
		if (str == null)
			return null;
		int beginIndex = 0;
		while (beginIndex < str.length() && isSpaceChar(str.charAt(beginIndex)))
			beginIndex++;
		int endIndex = str.length();
		while (endIndex > beginIndex && isSpaceChar(str.charAt(endIndex - 1)))
			endIndex--;
		return str.substring(beginIndex, endIndex);
	}

	public static boolean isSpaceChar(char ch) {
		return ch == ' ' // Space
				|| ch == '\t' // Horizontal Tabulator
				|| ch == '\u000B' // Vertical Tabulator
				|| ch == '\n'// Line Feed
				|| ch == '\r'// Carriage Return
				|| ch == '\u00A0' // No-Break Space
				|| ch == '\u1680' // Ogham Space Mark
				|| ch == '\u180E' // Mongolian Vowel Separator (MVS)
				|| ch == '\u2000' // En quad
				|| ch == '\u2001' // Em quad (Mutton quad)
				|| ch == '\u2002' // En Space (Nut)
				|| ch == '\u2003' // Em Space (Mutton)
				|| ch == '\u2004' // Three-Per-Em Space (Thick Space)
				|| ch == '\u2005' // Four-Per-Em Space (Mid Space)
				|| ch == '\u2006' // Six-Per-Em Space
				|| ch == '\u2007' // Figure Space
				|| ch == '\u2008' // Punctuation Space
				|| ch == '\u2009' // Thin Space
				|| ch == '\u200A' // Hair Space
				|| ch == '\u200B' // Zero Width Space (ZWSP)
				|| ch == '\u200C' // Zero Width Non Joiner (ZWNJ)
				|| ch == '\u200D' // Zero Width Joiner (ZWJ)
				|| ch == '\u2028' // Line Separator
				|| ch == '\u2029' // Paragraph Separator
				|| ch == '\u202F' // Narrow No-Break Space
				|| ch == '\u205F' // Medium Mathematical Space (MMSP)
				|| ch == '\u2060' // Word Joiner (WJ)
				|| ch == '\u2800' // Braille Pattern Blank
				|| ch == '\u3000' // Ideographic Space
				;
	}

	public static String trimToNull(String str) {
		if (str == null)
			return null;
		String trimmed = trim(str);
		if (trimmed.length() == 0)
			return null;
		return trimmed;
	}

	public static boolean startsWith(String text, String prefix) {
		if (text == null)
			return false;
		return text.startsWith(prefix);
	}

	public static boolean endsWith(String text, String suffix) {
		if (text == null)
			return false;
		return text.endsWith(suffix);
	}

	public static String removeApostrophes(String str) {
		str = trimToNull(str);
		if (str != null) {
			if (startsWith(str, "\""))
				str = str.substring(1);
			if (endsWith(str, "\""))
				str = str.substring(0, str.length() - 1);
		}
		return str;
	}

	public static Map<String, String> getRelations(String relationString) {
		Map<String, String> relations = new HashMap<>();
		if (relationString != null) {
			for (String relation : relationString.split(",")) {
				String pckgName = getPackageName(relation);
				String version = getVersion(relation); // may be null
				if (pckgName != null)
					relations.put(pckgName, version);
			}
		}
		return relations;
	}

	private static String getVersion(String relation) {
		String version = null;
		relation = trimToNull(relation);
		if (relation != null && relation.contains(">="))
			version = trim(relation.substring(relation.indexOf(">=") + 2, relation.indexOf(")")));
		return version;
	}

	private static String getPackageName(String relation) {
		relation = trimToNull(relation);
		if (relation != null && relation.contains("("))
			return trim(relation.substring(0, relation.indexOf("(")));
		return trim(relation);
	}

	public static String removeBackslashN(String str) {
		if (str != null) {
			str = str.replace("\\n", "");
		}
		return str;
	}

	public static String getExtension(String fileName) {
		String ext = null;
		if (fileName != null && fileName.contains(".")) {
			ext = fileName.substring(fileName.lastIndexOf('.') + 1);
		}
		return ext;
	}

	public static void updateProgress(double percentage) {
		updateProgress(percentage, true);
	}

	public static void updateProgress(double progressPercentage, boolean resetLine) {
		final int width = 50; // progress bar width in chars

		if (resetLine)
			System.out.print("\r");
		System.out.print("[");
		int i = 0;
		for (; i < (int) (progressPercentage * width); i++) {
			System.out.print("=");
		}
		for (; i < width; i++) {
			System.out.print(" ");
		}
		System.out.print("]");
		System.out.print(String.valueOf(Math.round(progressPercentage * 10000) / 100.f) + " %  ");
	}

	public static String prepareHTMLOutput(String logMessage) {
		if (logMessage != null) {
			logMessage = logMessage.replaceAll("\n", "<br/>").replaceAll("\t", "");
		}
		return logMessage;
	}
}
