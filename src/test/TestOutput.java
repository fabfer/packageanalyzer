package test;

import database.AnalyzedPackageTest.LOG_TYPE;

public class TestOutput {

	private long timeNeeded;
	private String logMessage;
	private LOG_TYPE severity;

	public TestOutput(String logMessage, LOG_TYPE severity) {
		this.logMessage = logMessage;
		this.severity = severity;
	}

	public String getLogMessage() {
		return logMessage;
	}

	public LOG_TYPE getSeverity() {
		return severity;
	}

	public void setTimeNeeded(long timeNeeded) {
		this.timeNeeded = timeNeeded;
	}
	
	public long getTimeNeeded() {
		return timeNeeded;
	}

	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}

}
