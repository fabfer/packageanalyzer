package test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import database.AnalyzedPackageTest.LOG_TYPE;
import database.RPackage;
import database.RPackageBluePrint;
import util.RUtil;
import util.TextUtil;

public class DependencyTest extends Test {

	private List<RPackage> sorted;

	public DependencyTest() {
		super("7de72b10-927c-11e5-a837-0800200c9a66", "Dependency test");
		this.sorted = new ArrayList<>();
	}

	/* use both r and fastr base packages, we do not want to distinguish here */
	@Override
	protected boolean isBuiltInPackage(RPackageBluePrint key) {
		return RUtil.fastrBasePackages.contains(key.getName()) || RUtil.rBasePackages.contains(key.getName());
	}

	@Override
	protected TestOutput analyze(File dir, RPackage pckg) {
		TestOutput output;
		if (sorted.contains(pckg)) {
			output = new TestOutput("All dependencies are valid", LOG_TYPE.OK);
		} else {
			StringBuilder result = new StringBuilder();
			result.append("<div style=\"position:relative\">");
			result.append("<h3>Problems</h3>");
			String reason = TextUtil.trimToNull(getReasonForFailure(pckg, sorted, "is not valid"));
			if (reason != null)
				result.append(reason);
			result.append("</div>");
			if (reason == null)
				output = new TestOutput("Dependencies are invalid, but no error could be detected", LOG_TYPE.WARNING);
			else
				output = new TestOutput(result.toString(), LOG_TYPE.ERROR);
		}
		return output;
	}

	@Override
	public void clean() {
		// nothing to clean
	}

	@Override
	public void setPackages(List<RPackage> packages) {
		if (packages != null) {
			sortByDependencies(packages, sorted);
			List<RPackage> missing = new ArrayList<>();
			for (RPackage pckg : packages) {
				if (!sorted.contains(pckg))
					missing.add(pckg);
			}
			packages = new ArrayList<>(sorted);
			packages.addAll(missing); // append missing/invalid packages
		}
		super.setPackages(packages);
	}

	private void sortByDependencies(List<RPackage> from, List<RPackage> to) {
		int added = 0;
		do {
			added = 0;
			for (RPackage pckg : from) {
				if (!to.contains(pckg) && pckg.onlyDependsOn(to)) {
					to.add(pckg);
					added++;
				}
			}
		} while (added > 0);
	}
}
