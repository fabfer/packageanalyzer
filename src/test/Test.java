package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import database.Analysis;
import database.AnalyzedPackage;
import database.AnalyzedPackageTest;
import database.AnalyzedPackageTest.LOG_TYPE;
import database.PackageAnalyzerDB;
import database.RPackage;
import database.RPackageBluePrint;
import database.SQLSession;
import util.TextUtil;

public abstract class Test {

	public static final List<Test> ROOT_TESTS = new ArrayList<>();
	public static final List<Test> ALL_TESTS = new ArrayList<>();

	public static final String FIELD_NAME_UUID = "uuid";
	public static final String FIELD_NAME_DESCRIPTION = "description";

	private static final Map<RPackage, AnalyzedPackage> cache = new HashMap<RPackage, AnalyzedPackage>();

	protected String uuid;
	protected String description;
	protected List<RPackage> packages;
	protected List<Test> dependingTests;
	protected Test parentTest;

	protected Test(String uuid, String description) {
		this.uuid = uuid;
		this.description = description;
		this.dependingTests = new ArrayList<>();
	}

	public void analyze(Analysis analysis, SQLSession sqlSession) {
		List<RPackage> passedPackages = new ArrayList<>();
		int count = 0;
		for (RPackage pckg : packages) {
			AnalyzedPackage analyzedPackage = cache.get(pckg);
			if (analyzedPackage == null) {
				analyzedPackage = AnalyzedPackage.getOrCreateInstance(sqlSession, analysis, pckg);
				analysis.addAnalyzedPackage(analyzedPackage);
				cache.put(pckg, analyzedPackage);
			}

			File file = new File("resources/packages/" + pckg.getLocalDirUrl());
			TestOutput result;
			long timeNeeded = System.currentTimeMillis();
			if (file.exists() && file.isDirectory())
				result = analyze(file, pckg);
			else
				result = new TestOutput("Package at resources/packages/" + pckg.getLocalDirUrl() + " not found", LOG_TYPE.ERROR);
			timeNeeded = System.currentTimeMillis() - timeNeeded;
			result.setTimeNeeded(timeNeeded);
			result.setLogMessage(TextUtil.prepareHTMLOutput(result.getLogMessage()));

			analyzedPackage.addAnalyzedPackageTest(AnalyzedPackageTest.createInstance(sqlSession, analyzedPackage, this, result));

			if (result.getSeverity() == LOG_TYPE.OK || result.getSeverity() == LOG_TYPE.NOTE) {
				passedPackages.add(pckg);
			}

			System.out.print("\rTesting " + description.substring(0, Math.min(50, description.length())) + ":  ");
			TextUtil.updateProgress(++count / (double) packages.size(), false);
			System.out.print(" " + pckg.getBlueprint().getName() + "                             ");
		}
		System.out.println("");
		for (Test t : dependingTests) {
			t.setPackages(passedPackages);
			t.analyze(analysis, sqlSession);
		}
	}

	protected abstract TestOutput analyze(File dir, RPackage rPackage);

	/**
	 * Each test should clean up. Should be called at the end of the program.
	 */
	public abstract void clean();

	public String getUuid() {
		return uuid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	// Used as label in the output UI, override if necessary
	public String getTestDescription() {
		return description;
	}

	public static Test getTest(String uuid) {
		for (Test t : ALL_TESTS) {
			if (t.uuid.equals(uuid))
				return t;
		}
		return null;
	}

	private static Test getTestByDescription(SQLSession sqlSession, String description, String regex, Class<RegexTest> clazz) {
		String sql = "SELECT * FROM " + PackageAnalyzerDB.NAME_TEST + " WHERE " + FIELD_NAME_DESCRIPTION + " = ?";
		ResultSet rs = null;
		Test test = null;
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, regex);
			rs = stmt.executeQuery();
			test = getTest(sqlSession, rs, clazz, description);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			PackageAnalyzerDB.close(rs);
		}
		return test;
	}

	private static Test getTest(SQLSession sqlSession, ResultSet rs, Class<? extends Test> clazz, String description)
			throws SQLException {
		Test test = null;
		if (rs != null && rs.next()) {
			try {
				if (PackageAnalyzerDB.NAME_TEST.equals(rs.getMetaData().getTableName(1).toLowerCase())) {
					test = clazz.getConstructor(String.class, String.class, String.class).newInstance(rs.getString(FIELD_NAME_UUID),
							rs.getString(FIELD_NAME_DESCRIPTION), description);
				}
			} catch (SQLException | InstantiationException | IllegalAccessException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
		return test;
	}

	public boolean persist(SQLSession sqlSession) {
		String sql = "INSERT INTO " + PackageAnalyzerDB.NAME_TEST + " VALUES (?, ?)";
		try (PreparedStatement stmt = sqlSession.getConnection().prepareStatement(sql)) {
			int index = 1;
			stmt.setString(index++, uuid);
			stmt.setString(index++, description);
			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 1) {
				sqlSession.commit();
				return true;
			}
		} catch (SQLException e) {
			// no output, will happen sometimes
		}
		return false;
	}

	protected void addDependingTest(Test t) {
		dependingTests.add(t);
		t.parentTest = this;
	}

	public static void initTests(SQLSession sqlSession) {
		Test ffcTest = new FileAndFolderCountTest();
		ROOT_TESTS.add(ffcTest);
		ALL_TESTS.add(ffcTest);
		Test extTest = new ExtensionsTest();
		ROOT_TESTS.add(extTest);
		ALL_TESTS.add(extTest);
		File regexes = new File(RegexTest.REGEX_FILE);
		if (regexes.exists()) {
			try (BufferedReader br = new BufferedReader(new FileReader(regexes))) {
				String line;
				while ((line = br.readLine()) != null) {
					String description = line;
					String regex = line;
					if (line.split(RegexTest.REGEX_SEPARATOR).length == 2) {
						description = TextUtil.trimToNull(line.split(RegexTest.REGEX_SEPARATOR)[0]);
						regex = TextUtil.trimToNull(line.split(RegexTest.REGEX_SEPARATOR)[1]);
					}
					Test t = getTestByDescription(sqlSession, description, regex, RegexTest.class);
					if (t == null)
						t = new RegexTest(UUID.randomUUID().toString(), description, regex);
					else {
						((RegexTest) t).setLabel(description);
						t.setDescription(regex);
					}
					ROOT_TESTS.add(t);
					ALL_TESTS.add(t);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		RVersionDependencyTest rVersionTest = new RVersionDependencyTest();
		ROOT_TESTS.add(rVersionTest);
		ALL_TESTS.add(rVersionTest);
		DependencyTest dpt = new DependencyTest();
		ALL_TESTS.add(dpt);
		rVersionTest.addDependingTest(dpt);
		RInstallTest rInstallTest = new RInstallTest();
		dpt.addDependingTest(rInstallTest);
		ALL_TESTS.add(rInstallTest);
		FastRInstallTest fastRInstallTest = new FastRInstallTest();
		rInstallTest.addDependingTest(fastRInstallTest);
		ALL_TESTS.add(fastRInstallTest);

		for (Test t : ALL_TESTS) {
			t.persist(sqlSession);
		}
	}

	protected String getReasonForFailure(RPackage rPackage, List<RPackage> success, String reasonString) {
		StringBuilder result = new StringBuilder();
		boolean hasReason = false;
		for (Entry<RPackageBluePrint, String> entry : rPackage.getDependsOn().entrySet()) {
			if (!isBuiltInPackage(entry.getKey())) {
				RPackage pckg = entry.getKey().findPackage(success);
				if (pckg == null) {
					if (!hasReason) {
						hasReason = true;
						result.append("<ul class=\"hierarchy\">");
					}
					pckg = entry.getKey().findPackage(getAllPackages());
					result.append("<li>");
					if (pckg == null) {
						result.append("Package \"");
						result.append(entry.getKey());
						result.append("\" is not available at the repository");
					} else {
						result.append("Depending package \"");
						result.append(pckg);
						result.append("\" ");
						result.append(reasonString);
						result.append(getReasonForFailure(pckg, success, reasonString));
					}
					result.append("</li>");
				}
			}
		}
		for (Entry<RPackageBluePrint, String> entry : rPackage.getImports().entrySet()) {
			if (!isBuiltInPackage(entry.getKey())) {
				RPackage pckg = entry.getKey().findPackage(success);
				if (pckg == null) {
					if (!hasReason) {
						hasReason = true;
						result.append("<ul class=\"hierarchy\">");
					}
					pckg = entry.getKey().findPackage(getAllPackages());
					result.append("<li>");
					if (pckg == null) {
						result.append("Package \"");
						result.append(entry.getKey());
						result.append("\" is not available at the repository");
					} else {
						result.append("Imported package \"");
						result.append(pckg);
						result.append("\" ");
						result.append(reasonString);
						result.append(getReasonForFailure(pckg, success, reasonString));
					}
					result.append("</li>");
				}
			}
		}
		if (hasReason)
			result.append("</ul>");
		return result.toString();
	}

	protected abstract boolean isBuiltInPackage(RPackageBluePrint key);

	public List<Test> getDependingTests() {
		return dependingTests;
	}

	public void setPackages(List<RPackage> packages) {
		this.packages = packages;
	}

	protected List<RPackage> getAllPackages() {
		if (parentTest != null)
			return parentTest.getAllPackages();
		return packages;
	}
}
