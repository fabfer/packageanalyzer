package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import database.AnalyzedPackageTest.LOG_TYPE;
import database.RPackage;
import database.RPackageBluePrint;

public class RegexTest extends Test {

	protected static final String REGEX_FILE = "resources/regexes.txt";
	protected static final String REGEX_SEPARATOR = "=>";
	
	private String label;
	private Pattern pattern;
	private CharsetDecoder decoder;

	public RegexTest(String uuid, String label, String regex) {
		super(uuid, regex);
		this.setLabel(label);
		pattern = Pattern.compile(getDescription());
		decoder = Charset.forName("ISO-8859-1").newDecoder();
	}

	@Override
	protected TestOutput analyze(File dir, RPackage pckg) {
		// we analyze the subdirectories /R and /src (if existing)
		List<String> srcFileMatchingRegex = new ArrayList<>();
		collectFileMatchingRegex("./R", new File(dir.getAbsolutePath() + "/R"), srcFileMatchingRegex);
		collectFileMatchingRegex("./R", new File(dir.getAbsolutePath() + "/src"), srcFileMatchingRegex);

		StringBuilder output = new StringBuilder("No files with content matching \"/" + getDescription() + "/\" found in package");
		if (srcFileMatchingRegex.size() > 0) {
			output = new StringBuilder();
			output.append("<div style=\"font-weight:bold\">Files matching \"/" + pattern + "/\"</div>");
			output.append("<ul>");
		}
		for (String file : srcFileMatchingRegex) {
			output.append("<li>" + file + "</li>");
		}
		if (srcFileMatchingRegex.size() > 0)
			output.append("</ul>");
		return new TestOutput(output.toString(), srcFileMatchingRegex.size() == 0 ? LOG_TYPE.ERROR : LOG_TYPE.OK);
	}

	private void collectFileMatchingRegex(String path, File file, List<String> srcFileMatchingRegex) {
		if (file.exists()) {
			if (file.isDirectory()) {
				for (File f : file.listFiles()) {
					collectFileMatchingRegex(path + "/" + f.getName(), f, srcFileMatchingRegex);
				}
			} else if (fileContainsRegex(file))
				srcFileMatchingRegex.add(path);
		}
	}

	private boolean fileContainsRegex(File f) {
		try (FileInputStream input = new FileInputStream(f);) {
			FileChannel channel = input.getChannel();
			ByteBuffer bbuf = channel.map(FileChannel.MapMode.READ_ONLY, 0, (int) channel.size());
			CharBuffer cbuf = decoder.decode(bbuf);
			Matcher matcher = pattern.matcher(cbuf);
			return matcher.find();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void clean() {
		// nothing to clean
	}

	@Override
	protected boolean isBuiltInPackage(RPackageBluePrint key) {
		return false;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String getTestDescription() {
		return label;
	}
}
