package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import database.AnalyzedPackageTest.LOG_TYPE;
import database.RPackage;
import database.RPackageBluePrint;
import util.CmdOutput;
import util.RUtil;
import util.TextUtil;

public class RInstallTest extends Test {

	private List<RPackage> installedSuccessfully;

	protected RInstallTest() {
		super("25a127fe-4504-453a-81b5-36986692c8a1", "Install packages in R");
		installedSuccessfully = new ArrayList<>();
	}

	@Override
	protected TestOutput analyze(File dir, RPackage pckg) {
		String output = null;
		LOG_TYPE type = LOG_TYPE.ERROR;
		if (pckg.onlyDependsOn(installedSuccessfully)) {
			if (!RUtil.neededRPackages.contains(pckg.getBlueprint().getName())) {
				try {
					CmdOutput result = RUtil.installLocalPackage(RUtil.RSCRIPT_CMD, dir.getName());
					output = TextUtil.trim(result.getOutput());
					if (result.wasInstalledSuccessfully(pckg.getBlueprint().getName())) {
						type = LOG_TYPE.OK;
						installedSuccessfully.add(pckg);
					}
				} catch (IOException | InterruptedException e) {
					output = e.getMessage();
				}
			} else {
				installedSuccessfully.add(pckg);
				output = "This packages is needed for running the analyzing tool";
				type = LOG_TYPE.NOTE;
			}
		} else {
			type = LOG_TYPE.WARNING;
			output = getReasonForFailure(pckg, installedSuccessfully, "was not installed successfully");
		}

		return new TestOutput(output, type);
	}

	@Override
	protected boolean isBuiltInPackage(RPackageBluePrint key) {
		return RUtil.rBasePackages.contains(key.getName());
	}

	@Override
	public void clean() {
		for (RPackage pckg : installedSuccessfully) {
			if (!RUtil.neededRPackages.contains(pckg.getBlueprint().getName())) {
				try {
					RUtil.uninstallPackage(RUtil.RSCRIPT_CMD, pckg);
				} catch (Exception e) {
					// ignore this one...
				}
			}
		}
	}

}
