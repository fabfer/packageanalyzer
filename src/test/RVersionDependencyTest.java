package test;

import java.io.File;

import database.AnalyzedPackageTest.LOG_TYPE;
import database.RPackage;
import database.RPackageBluePrint;
import util.RUtil;

public class RVersionDependencyTest extends Test {

	protected RVersionDependencyTest() {
		super("20a7fbdf-2131-488a-86c0-47ad76a29875", "May be installed in current R version");
	}

	@Override
	protected TestOutput analyze(File dir, RPackage rPackage) {
		TestOutput output = new TestOutput("OK", LOG_TYPE.OK);
		if (rPackage.needsNewRVersion(RUtil.R_VERSION)) {
			output = new TestOutput("Needs R version " + rPackage.getNeedsRVersion(), LOG_TYPE.ERROR);
		}
		return output;
	}

	@Override
	public void clean() {
		// nothing to clean
	}

	@Override
	protected boolean isBuiltInPackage(RPackageBluePrint key) {
		return false;
	}
}
