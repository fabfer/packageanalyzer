package test;

import java.io.File;

import database.AnalyzedPackageTest.LOG_TYPE;
import database.RPackage;
import database.RPackageBluePrint;

class FileAndFolderCountTest extends Test {

	public FileAndFolderCountTest() {
		super("b57d7670-77e6-11e5-a837-0800200c9a66", "File and folder count");
	}

	@Override
	protected TestOutput analyze(File dir, RPackage pckg) {
		int files = countFiles(dir);
		int folders = countFolders(dir);
		return new TestOutput("Contains " + folders + " folders and " + files + " files", LOG_TYPE.OK);
	}
	
	private int countFiles(File dir) {
		int files = 0;
		for (File f : dir.listFiles()) {
			if (f.isDirectory())
				files += countFiles(f);
			else
				files++;
		}
		return files;
	}
	
	private int countFolders(File dir) {
		int folders = 0;
		for (File f : dir.listFiles()) {
			if (f.isDirectory()) {
				countFolders(f);
				folders++;
			}
		}
		return folders;
	}

	@Override
	public void clean() {
		// nothing to clean
	}

	@Override
	protected boolean isBuiltInPackage(RPackageBluePrint key) {
		return false;
	}
}
