package test;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import database.AnalyzedPackageTest.LOG_TYPE;
import database.RPackage;
import database.RPackageBluePrint;
import util.TextUtil;

class ExtensionsTest extends Test {

	private static final Map<String, String> extensionMap = new HashMap<>();

	public ExtensionsTest() {
		super("f3a0b7b0-8ded-11e5-a837-0800200c9a66", "Distinct extensions of all source files");
		extensionMap.put("r", "R-File (.r)");
		extensionMap.put("cpp", "C++ (.cpp)");
		extensionMap.put("c", "C-File (.c)");
		extensionMap.put("h", "C-Header (.h)");
		extensionMap.put("o", "Object-File (.o)");
		extensionMap.put("txt", "Text-File (.txt)");
		extensionMap.put("f90", "Fortran-Program (.f90)");
		extensionMap.put("f", "Fortran-Source (.f)");
		extensionMap.put("html", "HTML-Datei (.html)");
		extensionMap.put("css", "Stylesheet (.css)");
		extensionMap.put("js", "Javascript (.js)");
	}

	@Override
	protected TestOutput analyze(File dir, RPackage pckg) {
		// we analyze the subdirectories /R and /src (if existing)
		Set<String> extensions = new HashSet<>();
		File r = new File(dir.getAbsolutePath() + "/R");
		File src = new File(dir.getAbsolutePath() + "/src");
		collectExtensions(r, extensions);
		collectExtensions(src, extensions);

		StringBuilder output = new StringBuilder("No source files found in package");
		if (extensions.size() > 0) {
			output = new StringBuilder();
			output.append("<div style=\"font-weight:bold\">Package contains</div>");
			output.append("<ul>");
		}
		for (String ext : extensions) {
			output.append("<li>" + ext + "</li>");
		}
		if (extensions.size() > 0)
			output.append("</ul>");
		return new TestOutput(output.toString(), extensions.size() > 0 ? LOG_TYPE.OK : LOG_TYPE.ERROR);
	}

	private void collectExtensions(File r, Set<String> extensions) {
		if (r != null && r.exists() && r.isDirectory()) {
			for (File f : r.listFiles()) {
				collectExtensions(f, extensions);
			}
		} else if (r != null && r.exists()) { // r is a file => get extension
			String ext = TextUtil.getExtension(r.getName());
			if (ext != null) {
				ext = ext.toLowerCase();
				extensions.add(extensionMap.containsKey(ext) ? extensionMap.get(ext) : ext);
			}
		}
	}

	@Override
	public void clean() {
		// nothing to clean
	}

	@Override
	protected boolean isBuiltInPackage(RPackageBluePrint key) {
		return false;
	}
}
