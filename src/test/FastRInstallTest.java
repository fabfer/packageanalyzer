package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import database.AnalyzedPackageTest.LOG_TYPE;
import database.RPackage;
import database.RPackageBluePrint;
import util.CmdOutput;
import util.CmdOutput.OUTPUT_TYPE;
import util.RUtil;
import util.TextUtil;

public class FastRInstallTest extends Test {

	private List<RPackage> installedSuccessfully;

	protected FastRInstallTest() {
		super("346b3620-9218-11e5-a837-0800200c9a66", "FastR install");
		installedSuccessfully = new ArrayList<>();
	}

	@Override
	protected TestOutput analyze(File dir, RPackage pckg) {
		String output = null;
		LOG_TYPE type = LOG_TYPE.ERROR;
		if (pckg.onlyDependsOn(installedSuccessfully)) {
			try {
				CmdOutput result = RUtil.installLocalPackage(RUtil.FASTRSCRIPT_CMD, dir.getName());
				output = TextUtil.trim(result.getOutput());
				if (result.wasInstalledSuccessfully(pckg.getBlueprint().getName())) {
					type = LOG_TYPE.OK;
					installedSuccessfully.add(pckg);
				}
			} catch (IOException | InterruptedException e) {
				output = e.getMessage();
			}
		} else {
			type = LOG_TYPE.WARNING;
			output = getReasonForFailure(pckg, installedSuccessfully, "was not installed successfully");
		}
		return new TestOutput(output, type);
	}

	@Override
	protected boolean isBuiltInPackage(RPackageBluePrint key) {
		return RUtil.fastrBasePackages.contains(key.getName());
	}

	@Override
	public void clean() {
		for (RPackage pckg : installedSuccessfully) {
			try {
				CmdOutput output = RUtil.uninstallPackage(RUtil.FASTRSCRIPT_CMD, pckg);
				if (output.getType() == OUTPUT_TYPE.ERROR)
					throw new Exception(output.getOutput());
			} catch (Exception e) {
				System.out.println("Error while removing package \"" + pckg.getBlueprint().getName() + "\"");
				System.out.println(e.getMessage());
			}
		}
	}

}
