package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import database.Analysis;
import database.ConnectionManager;
import database.RPackage;
import database.RPackageBluePrint;
import database.SQLSession;
import output.PackageAnalyzerHTMLPage;
import test.Test;
import util.RUtil;
import util.TextUtil;

public class PackageAnalyzer {

	private static final String NOW = "now";
	private static final String LAST = "last";

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");

	private static final Options options;
	private static final Option reason = Option.builder("r").argName("reason!").hasArg()
			.desc("Reason for the analysis, will be displayed in the output").build();
	private static final Option allPackages = Option.builder("a").argName("timestamp").hasArgs().optionalArg(true)
			.desc("Selects all packages for analysis. May specify a timestamp which fetches all packages from the most recent analysis BEFORE this date.")
			.build();
	private static final Option onlyNewPackages = Option.builder("n")
			.desc("Selects all packages that were updated since the last analysis.").build();
	private static final Option singlePackage = Option.builder("s").argName("name!> <timestamp").hasArgs()
			.desc("Selects a single package with name <name> for analysis. May specify a timestamp which fetches the most recent package BEFORE this date.")
			.build();
	private static final Option compare = Option.builder("c").argName("timestamp").hasArgs().valueSeparator(',')
			.desc("Compares two or more analyses by specifying timestamps. Multiple values are separated by ','. To compare with current packages write 'now'. E.g. -c now, last, \"20121212 133700\"")
			.build();

	static {
		RUtil.neededRPackages.add("XML");
		options = new Options();
		options.addOption(reason);
		options.addOption(allPackages);
		options.addOption(onlyNewPackages);
		options.addOption(singlePackage);
		options.addOption(compare);
	}

	public static final List<RPackage> oldPackages = new ArrayList<RPackage>();
	public static final List<RPackage> newPackages = new ArrayList<RPackage>();

	public static void main(String[] args) throws Exception {
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(options, args);

		String analysisReason = "No reason specified";
		if (cmd.hasOption(reason.getOpt()))
			analysisReason = cmd.getOptionValue(reason.getOpt());

		SQLSession sqlSession = null;

		try {
			sqlSession = ConnectionManager.beginSession();

			if (cmd.hasOption(compare.getOpt())) { // compare multiple analysis
				initTests(sqlSession);
				checkRequirements();
				compare(cmd, analysisReason, sqlSession);
			} else if (cmd.hasOption(allPackages.getOpt())) { // analyze all packages
				initTests(sqlSession);
				checkRequirements();
				analyzeAllPackages(cmd, analysisReason, sqlSession);
			} else if (cmd.hasOption(onlyNewPackages.getOpt())) {
				initTests(sqlSession);
				checkRequirements();
				initAndUpdateCurrentPackages(sqlSession);
				if (newPackages.size() > 0)
					analyze(analysisReason, sqlSession, newPackages);
				else
					System.out.println("No new packages found");
			} else if (cmd.hasOption(singlePackage.getOpt())) { // analyze only a single package (and its dependencies)
				initTests(sqlSession);
				checkRequirements();
				analyzeSinglePackage(cmd, analysisReason, sqlSession);
			} else {
				printHelp(options);
			}
		} catch (SQLException | InterruptedException e) {
			e.printStackTrace();
			System.out.print("Cleaning tests... ");
			for (Test test : Test.ALL_TESTS) {
				test.clean();
			}
			System.out.println("[done]");
		} finally {
			sqlSession.close();
		}
		System.out.println("");
	}

	private static void initTests(SQLSession sqlSession) {
		System.out.print("Initializing tests ...");
		Test.initTests(sqlSession);
		System.out.println(" [done]");
	}

	private static void analyzeSinglePackage(CommandLine cmd, String analysisReason, SQLSession sqlSession) throws Exception {
		List<RPackage> packagesToAnalyze = new ArrayList<>();
		String[] params = cmd.getOptionValues(singlePackage.getOpt());
		if (params != null && params.length > 0) {
			String name = params[0];
			Date d = null;
			if (params.length > 1)
				d = getDateTime(params[1]);
			initAndUpdateCurrentPackages(sqlSession);
			RPackage pckg = RPackage.getPackageByName(name, d, sqlSession);
			if (pckg == null)
				throw new Exception("Could not find package with name '" + name + "'" + (d != null ? " before the date " + d : ""));

			packagesToAnalyze.add(pckg);
			analyze(analysisReason, sqlSession, packagesToAnalyze);
		} else {
			printHelp(options);
			throw new Exception("No name for package specified");
		}
	}

	private static void analyzeAllPackages(CommandLine cmd, String analysisReason, SQLSession sqlSession) throws Exception {
		List<RPackage> packagesToAnalyze = new ArrayList<>();
		String[] params = cmd.getOptionValues(allPackages.getOpt());
		if (params != null && params.length > 0) {
			if (params.length == 1 && LAST.equals(params[0])) {
				Analysis a = Analysis.getLastInstanceBeforeNow(sqlSession);
				if (a != null) {
					packagesToAnalyze = a.getPackages();
				} else
					throw new Exception("There are no analyses in the DB yet");
			} else {
				Date d = getDateTime(params[0]);
				if (d != null) {
					Analysis a = Analysis.getLastInstanceBeforeDate(sqlSession, d);
					if (a != null) {
						packagesToAnalyze = a.getPackages();
					} else
						throw new Exception("There is no analysis before " + d);
				} else
					throw new Exception("Invalid parameters");
			}
		} else {
			prepareAllPackages(sqlSession, packagesToAnalyze);
		}
		analyze(analysisReason, sqlSession, packagesToAnalyze);
	}

	private static void compare(CommandLine cmd, String analysisReason, SQLSession sqlSession) throws Exception {
		List<RPackage> packagesToAnalyze = new ArrayList<>();
		String[] params = cmd.getOptionValues(compare.getOpt());
		if (params != null) {
			boolean containsNow = false;
			List<Analysis> oldAnalysis = new ArrayList<>();
			for (int i = 0; i < params.length; i++) {
				String param = TextUtil.trimToNull(params[i]);
				if (param != null) {
					switch (param) {
					case NOW:
						containsNow = true;
						break;
					case LAST:
						Analysis last = Analysis.getLastInstanceBeforeNow(sqlSession);
						if (last != null)
							oldAnalysis.add(last);
						else
							throw new Exception("There are no analysis in the DB yet");
						break;
					default:
						Date d = null;
						if ((d = getDateTime(params[i])) != null) {
							Analysis old = Analysis.getLastInstanceBeforeDate(sqlSession, d);
							if (old != null)
								oldAnalysis.add(old);
							else
								throw new Exception("There is no analysis before " + d);
						}
						break;
					}
				}
			}

			initAndUpdateCurrentPackages(sqlSession);
			// check if user specified to compare only certain packages (-a or -s)
			// default is to analyze all packages from most recent analysis
			// e.g. "-c now, last" takes packages from last analysis
			if (cmd.hasOption(allPackages.getOpt())) {
				packagesToAnalyze.addAll(newPackages);
				packagesToAnalyze.addAll(oldPackages);
			} else if (cmd.hasOption(onlyNewPackages.getOpt())) {
				if (newPackages.size() > 0)
					packagesToAnalyze.addAll(newPackages);
				else
					throw new Exception("No new packages found");
			} else if (cmd.hasOption(singlePackage.getOpt())) {
				params = cmd.getOptionValues(singlePackage.getOpt());
				// first param is required to be the name of the package
				RPackage pckg = RPackage.getPackageByName(params[0], null, sqlSession);
				if (pckg == null)
					throw new Exception("Could not find package with name '" + params[0] + "'");
				packagesToAnalyze.add(pckg);
			} else if (oldAnalysis.size() >= 1) {
				packagesToAnalyze.addAll(oldAnalysis.get(0).getPackages());
			}

			if (containsNow && oldAnalysis.size() >= 1 || oldAnalysis.size() > 1) {
				if (!containsNow) { // compare already existing analysis
					PackageAnalyzerHTMLPage outputPage = new PackageAnalyzerHTMLPage(oldAnalysis);
					try {
						outputPage.buildAndShow();
					} catch (IOException e) {
						throw new Exception("Could not build output page");
					}
				} else { // make analysis of new packages and then compare
					analyze(analysisReason, sqlSession, packagesToAnalyze, oldAnalysis.toArray(new Analysis[0]));
				}
			} else
				throw new Exception("Could not perform a compare. Too few analyses specified.");
		} else {
			printHelp(options);
			throw new Exception("No parameters for comparison specified");
		}
	}

	private static void prepareAllPackages(SQLSession sqlSession, List<RPackage> packagesToAnalyze)
			throws IOException, InterruptedException, MalformedURLException, FileNotFoundException {
		initAndUpdateCurrentPackages(sqlSession);
		packagesToAnalyze.addAll(newPackages);
		packagesToAnalyze.addAll(oldPackages);
	}

	private static void printHelp(final Options options) {
		System.out.println("No valid arguments specified");
		HelpFormatter formatter = new HelpFormatter();
		System.out.println("Parameters marked with '!' are mandatory");
		System.out.println("Parameters containing spaces must be surrounded by \"\"");
		System.out.println("Timestamps are specified in format YYYYMMDD [HH:mm:ss]");
		System.out.println("For timestamp in Option -a and -c you can use 'last' to select the last analysis");
		formatter.printHelp("packageanalyzer", options);
	}

	private static void checkRequirements() throws Exception {
		// first, check if all required programs are installed
		System.out.println("Checking requirements ... ");
		System.out.print("Checking Rscript ...");
		RUtil.checkIfInstalled(RUtil.RSCRIPT_CMD);
		System.out.println(" [ok]");
		System.out.print("Checking fastrscript ...");
		RUtil.checkIfInstalled(RUtil.FASTRSCRIPT_CMD);
		System.out.println(" [ok]");
		System.out.println("R base packages ...");
		RUtil.rBasePackages.addAll(RUtil.getBasePackages(RUtil.RSCRIPT_CMD));
		System.out.println(RUtil.rBasePackages);
		System.out.println("FastR base packages ...");
		RUtil.fastrBasePackages.addAll(RUtil.getBasePackages(RUtil.FASTRSCRIPT_CMD));
		System.out.println(RUtil.fastrBasePackages);
		System.out.println("Loading versions ...");
		RUtil.loadVersions();
		System.out.println("R version: " + RUtil.R_VERSION);
		System.out.println("FastR version: " + RUtil.FAST_R_VERSION);
		File pckFolder = new File(RUtil.PACKAGE_LOCATION);
		if (!pckFolder.exists())
			pckFolder.mkdirs();
		File rLib = new File(RUtil.R_LIB);
		if (!rLib.exists())
			rLib.mkdirs();
		File fastRLib = new File(RUtil.FASTR_LIB);
		if (!fastRLib.exists())
			fastRLib.mkdirs();
		for (String pckg : RUtil.neededRPackages) {
			RUtil.checkIfPackageIsInstalled(RUtil.RSCRIPT_CMD, pckg);
		}
		System.out.println("[done]");
	}

	private static void analyze(String analysisReason, SQLSession sqlSession, List<RPackage> packagesToAnalyze,
			Analysis... oldAnalysis) {
		addAllDependingPackages(packagesToAnalyze, sqlSession);
		Collections.sort(packagesToAnalyze, new Comparator<RPackage>() {
			@Override
			public int compare(RPackage pckg1, RPackage pckg2) {
				return pckg1.getBlueprint().getName().compareTo(pckg2.getBlueprint().getName());
			}
		});

		for (Test test : Test.ROOT_TESTS) {
			test.setPackages(packagesToAnalyze);
		}

		System.out.println("Testing packages ...");

		Analysis analysis = Analysis.createInstance(sqlSession, analysisReason);

		for (Test test : Test.ROOT_TESTS) {
			test.analyze(analysis, sqlSession);
		}

		System.out.println("");
		System.out.println("Creating the report");
		PackageAnalyzerHTMLPage outputPage = new PackageAnalyzerHTMLPage(analysis, oldAnalysis);
		try {
			outputPage.buildAndShow();
		} catch (IOException e) {
			System.out.println("Could not build output page");
			e.printStackTrace();
			System.out.print("Cleaning up database... ");
			analysis.delete(sqlSession);
			System.out.println("[done]");
		}

		// After testing, clean
		System.out.print("Cleaning installed packages... ");
		for (Test test : Test.ALL_TESTS) {
			test.clean();
		}
		System.out.println("[done]");
	}

	private static void initAndUpdateCurrentPackages(SQLSession sqlSession)
			throws IOException, InterruptedException, MalformedURLException, FileNotFoundException {
		System.out.println("Getting newest package data ...");
		RUtil.loadPackages(sqlSession);
		System.out.println("Downloading new packages ...");
		downloadMissingPackages(sqlSession);
	}

	private static Date getDateTime(String dateOrDatetime) throws ParseException {
		Date d = null;
		String p = TextUtil.trimToNull(dateOrDatetime);
		if (p != null) {
			if (p.contains(" "))
				d = dateTimeFormat.parse(p);
			else
				d = dateFormat.parse(p);
		}
		return d;
	}

	private static void addAllDependingPackages(List<RPackage> packagesToAnalyze, SQLSession sqlSession) {
		Set<RPackage> packagesToAdd = new HashSet<RPackage>();
		for (RPackage pckg : packagesToAnalyze) {
			for (Entry<RPackageBluePrint, String> pckgBlueprint : pckg.getDependsOn().entrySet())
				packagesToAdd.addAll(getDependingPackages(pckgBlueprint, sqlSession));
			for (Entry<RPackageBluePrint, String> pckgBlueprint : pckg.getImports().entrySet())
				packagesToAdd.addAll(getDependingPackages(pckgBlueprint, sqlSession));
		}
		for (RPackage pckg : packagesToAdd)
			if (!packagesToAnalyze.contains(pckg))
				packagesToAnalyze.add(pckg);
	}

	private static Set<RPackage> getDependingPackages(Entry<RPackageBluePrint, String> pckgBlueprint, SQLSession sqlSession) {
		Set<RPackage> packages = new HashSet<>();
		RPackage pckg = RPackage.getPackage(sqlSession, pckgBlueprint.getKey().getUuid(), pckgBlueprint.getValue());
		if (pckg != null) {
			packages.add(pckg);
			for (Entry<RPackageBluePrint, String> blueprint : pckg.getDependsOn().entrySet())
				packages.addAll(getDependingPackages(blueprint, sqlSession));
			for (Entry<RPackageBluePrint, String> blueprint : pckg.getImports().entrySet())
				packages.addAll(getDependingPackages(blueprint, sqlSession));
		}
		return packages;
	}

	private static void downloadMissingPackages(SQLSession sqlSession)
			throws MalformedURLException, FileNotFoundException, IOException, InterruptedException {
		int count = 0;
		System.out.println("--> Checking old packages");
		// check if packages in db are really downloaded
		for (RPackage pckg : oldPackages) {
			TextUtil.updateProgress(++count / (double) oldPackages.size());
			if (!pckg.existsLocally())
				RUtil.downloadPackage(pckg);
		}
		System.out.println("");

		count = 0;
		System.out.println("--> Checking newest packages");
		// download newest packages
		for (RPackage pckg : newPackages) {
			TextUtil.updateProgress(++count / (double) newPackages.size());
			if (!pckg.existsLocally())
				RUtil.downloadPackage(pckg);
		}
		System.out.println("");
		System.out.println("[done]");
	}

}
