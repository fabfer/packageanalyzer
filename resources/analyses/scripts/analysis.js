var logTypes = ["OK", "NOTE", "WARNING", "ERROR"];

var currentTestFilter;
var currentResultFilter;
var currentSearchFilter;

var currentUpdateRequest;

$(document).ready(function() {
	var firstTest = $("#tests li:first-child");
	var firstResultType = $("#log-types div:nth-child(2)");
	filterTest(firstTest);
	filterResult(firstResultType);
	startUpdate();
});

function openDialog(button, test) {
	var dialog = $("<div/>");
	dialog.addClass("dialog");
	dialog.click(function() { closeDialog(dialog, event) });
	var innerDialog = $("<div/>");
	innerDialog.click(function() { stopPropagation(event) });
	dialog.append(innerDialog);
	var time = $("<div/>");
	time.addClass("time-div");
	innerDialog.append(time);
	var logContent = $("<div/>");
	logContent.addClass("log-container-container");
	innerDialog.append(logContent);
	var timeImg = $("<img/>");
	timeImg.attr("src", "img/ic_time.png");
	timeImg.addClass("time-img");
	time.append(timeImg);
	time.append(test['time']);
	var log = $("<div/>");
	log.addClass("log-container");
	log.append("<div class='log-type " + test['logtype'] + " dot'/>");
	log.append("<div>" + test['logtype'] + "</div>");
	log.append(test['log'].replace(/(?:\r\n|\r|\n)/g, '<br />'));
	logContent.append(log);
	if (test['old']) {
		var index = 0;
		for (var oldVersion in test['old']) {
			var oldTest = test['old'][oldVersion];
			if (typeof oldTest === 'object') {
				time.append(" / ");
				var span = $("<span/>");
				span.addClass("old" + index);
				span.append(oldTest['time']);
				time.append(span);
				if ("log" in oldTest) {
					log = $("<div/>");
					log.addClass("log-container");
					log.addClass("old" + index);
					var logtype = ("logtype" in oldTest) ? oldTest['logtype'] : test['logtype'];
					log.append("<div class='log-type " + logtype + " dot'/>");
					log.append("<div>" + logtype + "</div>");
					log.append(oldTest['log'].replace(/(?:\r\n|\r|\n)/g, '<br />'));
					logContent.append(log);
				}
			}
			index++;
		}
	}
	$("body").append(dialog);
}

function closeDialog(div, event) {
	$(div).remove();
	stopPropagation(event);
}

function filterTest(li) {
	var filter = $(li).attr("id");
	selectItem(li);
	if (currentTestFilter != filter) {
		currentTestFilter = filter;
	}
}

function selectItem(item) {
	$(item).siblings().removeClass("selected");
	$(item).addClass("selected");
}

function deselectItem(item) {
	$(item).removeClass("selected");
}

function filterResult(span) {
	var filter = $(span).attr('id');
	if (currentResultFilter != filter) {
		currentResultFilter = filter;
		selectItem(span);
	} else {
		currentResultFilter = null;
		deselectItem(span);
	}
}

function stopPropagation(event) {
   if (event.stopPropagation) {
       event.stopPropagation();
   } else if (window.event) {
      window.event.cancelBubble=true;
   }
}

function updateAll() {
	var totalCounts = {};
	var typeCounts = {};
	var content = $("#content");
	content.empty();
	var onlyDiff = $("#onlyDiff");
	var onlyNew = $("#onlyNew");
		
	for (var packageName in packages) {
  		if (packages.hasOwnProperty(packageName)) {
    		var c = packages[packageName];
    		var version = c['version'];
    		for (var testId in c) {
    			if (!(testId in totalCounts))
					totalCounts[testId] = 0;
    			if (c.hasOwnProperty(testId)) {
					var test = c[testId];
					if (typeof test === 'object') {
						if (!currentSearchFilter || packageName.indexOf(currentSearchFilter) >= 0 || packageMatchesSearch(test)) {
							var isNewPackage = !("old" in test);
							if (!onlyNew || !onlyNew.prop("checked") || (onlyNew.prop("checked") && isNewPackage)) {
								var differs = false;
								if (onlyDiff)
									differs = differsFromOld(test);
								if (!onlyDiff || !onlyDiff.prop("checked") || (onlyDiff.prop("checked") && differs)) {
									if (currentTestFilter && testId === currentTestFilter) {
										var logtype = test['logtype'];
										if (!currentResultFilter || currentResultFilter === logtype) {
											// create element and append to content
											var div = $("<div/>");
											div.addClass("pckg");
											div.addClass(logtype);
											if (differs) {
												div.addClass("diff");
												$("#" + testId).addClass("diff");
												$("#" + logtype).addClass("diff");
											}
											if (onlyNew && onlyNew.prop("checked") && isNewPackage) {
												div.addClass("new-package");
												$("#" + testId).addClass("new-package");
												$("#" + logtype).addClass("new-package");
											}
											div.click((function(dv, tst) {
													return function() { openDialog(dv, tst); }
											})(div, test));
											content.append(div);
											var table = $("<table/>");
											div.append(table);
											var tbody = $("<tbody/>");
											table.append(tbody);
											var tr = $("<tr/>");
											tbody.append(tr);
											var td = $("<td/>");
											if (test['old'])
												td.attr("rowspan", Object.keys(test['old']).length + 1);
											td.text(packageName);
											td.css("padding-right", "10px");
											tr.append(td);
											td = $("<td/>");
											td.text(version);
											tr.append(td);
											var versions = [];
											versions.push(version);
											if (test['old']) {
												for (var oldVersion in test['old']) {
													if ($.inArray(oldVersion, versions) < 0) {
														tr = $("<tr/>");
														tbody.append(tr);
														td = $("<td/>");
														td.text(oldVersion);
														tr.append(td);
														versions.push(oldVersion);
													}
												}
											}
										}
										if (!(logtype in typeCounts))
											typeCounts[logtype] = 1;
										else
											typeCounts[logtype] = typeCounts[logtype] + 1;
									}
									if (!(testId in totalCounts))
										totalCounts[testId] = 1;
									else
										totalCounts[testId] = totalCounts[testId] + 1;
								}
							}
						}
					}
    			}
    		}
  		}
	}
	
	$.each(logTypes, function (index, value) {
		$("#" + value + " .count").text(typeCounts[value] ? typeCounts[value] : 0);
	});
	$.each(totalCounts, function (key, value) {
		$("#" + key + " .count").text(value);
	});
}

function differsFromOld(test) {
	if (test['old']) {
		for (var oldVersion in test['old']) {
			var oldTest = test['old'][oldVersion];
			if (typeof oldTest === 'object' && ("log" in oldTest)) {
				if (oldTest['log'] != test['log'])
					return true;
			}
		}
	}
	return false;
}

function packageMatchesSearch(test) {
	var result = test['log'].indexOf(currentSearchFilter) >= 0;
	if (!result)
		result = test['logtype'].indexOf(currentSearchFilter) >= 0;
	if (!result && test['old']) {
		for (var oldVersion in test['old']) {
			var oldTest = test['old'][oldVersion];
			if (typeof oldTest === 'object' && ('log' in oldTest)) {
				result = oldTest['log'].indexOf(currentSearchFilter) >= 0;
				if (result)
					break;
			}
			if ('logtype' in oldTest) {
				result = oldTest['logtype'].indexOf(currentSearchFilter) >= 0;
				if (result)
					break;
			}
		}
	}
	return result;
}

function endsWith(string, pattern) {
	var d = string.length - pattern.length;
    return d >= 0 && string.lastIndexOf(pattern) === d;
}

function startUpdate() {
	stop();
	start();
}

function updateSearch(input) {
	var text = $(input).val();
	currentSearchFilter = text;
	startUpdate();
}

function start() {
	if (!currentUpdateRequest)
		currentUpdateRequest = setTimeout(updateAll, 0);
	else
		stop();
}

function stop() {
	clearTimeout(currentUpdateRequest);
	currentUpdateRequest = null;
}