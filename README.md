# Package-Analyzer #

## Requirements ##
* Make sure R is installed and added to your PATH
* You need to install [FastR](https://bitbucket.org/allr/fastr/overview)
* Add the environment variable ```$FASTR_HOME```, which refers to your installed FastR folder
* Get this project and add its´ root directory to your PATH

## How to use the Package-Analyzer ##
### Regex-Search ###
To search for special content in packages, you may want to use regexes. Therefore edit the file ```resources/regexes.txt```. Examples are provided within the file.
### Use the command line ###
Open a terminal and change to the root directory of the Package-Analyzer. To run the program type:
```
#!

java -cp bin:lib/derby.jar:lib/commons-io-2.4.jar:lib/commons-lang3-3.4.jar:lib/commons-cli-1.3.1.jar main.PackageAnalyzer
```
Executing this command will display all possible input parameters. For an initial analysis you may want to test all packages, therefore use the parameter ```-a```, which will download and test all current R packages (as can be seen [here](https://cran.r-project.org/src/contrib/)). This might take the whole day.